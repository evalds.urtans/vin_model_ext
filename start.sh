#!/bin/sh -v

module load conda
export TMPDIR=$HOME/tmp
source activate conda_env

python taskgen.py \
-report test_1 \
-params_grid learning_rate batch_size \
-learning_rate 1e-2 1e-3 1e-4 \
-batch_size 64 128 \
-repeat 1 \
-process_count_per_task 1 \
-hpc_gpu_count 1 \
-is_hpc True \
-tf_cuda True \
-conda_env conda_env \
-is_debug True

# !!!! is_debug
# qstat
# qdel 97690.rudens
# qdel all
# qdel {97690..97690}.rudens
# showq -r   << izpildas tikai uz UI
# ssh wn58
# htop
# nvidia-smi