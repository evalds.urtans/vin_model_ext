In progress..

Run with command:  
`python ./main.py -path_value_maps /path/to/value_maps -model model_cnn_simple`

Much faster with GPU/CUDA:  
`-is_cuda true`
   
Start tensorboard with cmd:  
`tensorboard --logdir ./runs --port 8080`

Look at results:  
`http://localhost:8080`

Usage of task generator:
`python ./taskgen.py -report tests_1 -conda_env env1 -path_value_maps /path/to/value_maps -params_grid dropout learning_rate -learning_rate 1e-4 1e-5 -dropout 0.5 0.3 0.1 -batch_size 64 128 -reg_beta 0.01`

2. msvcrt.locking(f.fileno(), msvcrt.LK_NBLCK, 1)
    PermissionError: [Errno 13] Permission denied
