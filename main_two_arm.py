import os
import torch
import torch.nn
import torch.utils.data
import torch.onnx as torch_onnx
import tensorboardX
# conda install tensorflow
# conda install -c conda-forge tensorboardx
import torchnet as tnt
# pip install git+https://github.com/pytorch/tnt.git@master
import numpy as np
import time
import argparse
import json
import copy

from datetime import datetime

from modules.file_utils import FileUtils
from modules.tensorboard_utils import TensorBoardUtils
from modules.logging_utils import LoggingUtils
from modules.args_utils import ArgsUtils
from modules.csv_utils import CsvUtils
from modules.command_txt_utils import CommandTxtUtils
from modules.metrics_success_score import  Metrics_success_score
from modules.lr_scheduler import CosineWithRestarts
from modules.not_used.model_cnn_simple import Model
from modules.data_loader_cnn_simple import DataLoader

# ONLY FOR TWO ARM RNN
if __name__ == '__main__': # avoid problems with spawned processes
    parser = argparse.ArgumentParser(description='Model trainer')

    parser.add_argument('-is_cuda', default=False, type=lambda x: (str(x).lower() == 'true'))

    parser.add_argument('-id', default=0, type=int)
    parser.add_argument('-repeat_id', default=0, type=int)
    parser.add_argument('-report', default='summary', type=str)
    parser.add_argument('-params_report', nargs='*', required=False) # extra params for report global for series of runs
    parser.add_argument('-params_report_local', nargs='*', required=False) # extra params for local run report

    parser.add_argument('-name', help='Run name, by default date', default='', type=str)

    parser.add_argument('-epochs', default=10, type=int)
    parser.add_argument('-model', default='model_cnn_rnn_two_arm', type=str)

    parser.add_argument('-data_loader', default='data_loader_rnn_delta_two_arm', type=str)
    parser.add_argument('-is_data_normalization', default=True, type=lambda x: (str(x).lower() == 'true'))
    parser.add_argument('-is_debug_data_loader', default=False, type=lambda x: (str(x).lower() == 'true'))

    parser.add_argument('-path_value_maps', default='../VIN_Research_ValueIterationGridmap/valueMaps', type=str)

    parser.add_argument('-learning_rate', default=1e-4, type=float)
    parser.add_argument('-optimizer', default='adam', type=str)
    parser.add_argument('-sgd_momentum', default=0.9, type=float)
    parser.add_argument('-reg_beta', default=0, type=float)
    parser.add_argument('-is_loss_sum_batch', default=False, type=lambda x: (str(x).lower() == 'true'))
    parser.add_argument('-dropout', default=0.3, type=float)
    parser.add_argument('-lr_scheduler', default='', type=str)
    parser.add_argument('-lr_gamma', default=0.1, type=float)
    parser.add_argument('-lr_step_size', default=7, type=int)

    parser.add_argument('-loss', default='mse', help='mse msa hinge mse_custom_goal', type=str)
    parser.add_argument('-loss_custom_coef', default=0.1, type=float)

    parser.add_argument('-batch_size', default=64, type=int)

    parser.add_argument('-tensorboard_maps_count', default=10, type=int)

    parser.add_argument('-rnn_convergence_limit', default=0.01, type=int)

    parser.add_argument('-early_stopping_patience', default=3, type=int)
    parser.add_argument('-early_stopping_param', default='loss', type=str)
    parser.add_argument('-early_stopping_delta_percent', default=0.05, type=float)

    parser.add_argument('-is_add_graph_to_tensorboard', default=False, type=lambda x: (str(x).lower() == 'true'))
    parser.add_argument('-is_save_onnx_graph', default=False, type=lambda x: (str(x).lower() == 'true'))

    parser.add_argument('-is_residual', default=False, type=lambda x: (str(x).lower() == 'true'))
    parser.add_argument('-preload_weights_filename', default="none", type=str)

    parser.add_argument('-is_metric_success_rate', default=True, type=lambda x: (str(x).lower() == 'true'))
    parser.add_argument('-metric_success_rate_threads', default=8, type=int)
    parser.add_argument('-is_metric_score', default=False, type=lambda x: (str(x).lower() == 'true'))
    parser.add_argument('-is_metric_speed', default=True, type=lambda x: (str(x).lower() == 'true'))

    parser.add_argument('-is_quick_test', default=False, type=lambda x: (str(x).lower() == 'true')) # train/test only on a single batch

    parser.add_argument('-rnn_layer_count', default=1, type=int)
    parser.add_argument('-compression_fc', default=0.1, type=float)
    parser.add_argument('-init_values', default='old', type=str)

    parser.add_argument('-init_bias', default='zeros', type=str)
    parser.add_argument('-init_weights', default='xavier_normal', type=str)
    parser.add_argument('-init_bn', default='uniform', type=str)

    args, args_other = parser.parse_known_args()

    # torch.set_num_threads(16)

    tmp = ['id', 'name', 'repeat_id', 'loss', 'test_loss', 'best_loss', 'avg_epoch_time']
    if args.is_metric_success_rate:
        tmp += ['success_rate', 'best_success_rate']
    if args.is_metric_score:
        tmp += ['score', 'best_score']
    if args.is_metric_speed:
        tmp += ['speed', 'avg_speed']
    if not args.params_report is None:
        for it in args.params_report:
            if not it in tmp:
                tmp.append(it)
    args.params_report = tmp

    tmp = ['epoch', 'loss', 'test_loss', 'epoch_time', 'test_loss', 'best_loss', 'avg_epoch_time']
    if args.is_metric_success_rate:
        tmp += ['success_rate', 'best_success_rate']
    if args.is_metric_score:
        tmp += ['score', 'best_score']
    if args.is_metric_speed:
        tmp += ['speed', 'avg_speed']
    if not args.params_report is None:
        for it in args.params_report:
            if not it in tmp:
                tmp.append(it)
    args.params_report = tmp

    tmp = ['epoch', 'loss', 'test_loss', 'epoch_time', 'early_percent_improvement']
    if args.is_metric_success_rate:
        tmp += ['success_rate']
    if args.is_metric_score:
        tmp += ['score']
    if args.is_metric_speed:
        tmp += ['speed']
    if not args.params_report_local is None:
        for it in args.params_report_local:
            if not it in tmp:
                tmp.append(it)
    args.params_report_local = tmp

    FileUtils.createDir('./reports')
    filename = os.path.join('reports', args.report) + '.csv'
    if not os.path.exists(filename):
        with open(filename, 'w') as outfile:
            FileUtils.lock_file(outfile)
            outfile.write(','.join(args.params_report) + '\n')
            FileUtils.unlock_file(outfile)


    if len(args.name) == 0:
        args.name = datetime.now().strftime('%y-%m-%d_%H-%M-%S')

    run_path = './runs/' + args.name
    if os.path.exists(run_path):
        FileUtils.deleteDir(run_path, is_delete_dir_path=True)
    FileUtils.createDir(run_path)

    if not torch.cuda.is_available():
        args.is_cuda = False

    tensorboard_writer = tensorboardX.SummaryWriter(log_dir=run_path)
    tensorboard_utils = TensorBoardUtils(tensorboard_writer)
    logging_utils = LoggingUtils(filename=os.path.join(run_path, 'log.txt'))
    ArgsUtils.log_args(args, 'main.py', logging_utils)

    meter_loss = tnt.meter.AverageValueMeter()
    meter_test_loss = tnt.meter.AverageValueMeter()
    meter_success_rate = tnt.meter.AverageValueMeter()
    meter_score = tnt.meter.AverageValueMeter()
    meter_success_rate_real = tnt.meter.AverageValueMeter()
    meter_score_real = tnt.meter.AverageValueMeter()
    meter_speed = tnt.meter.AverageValueMeter()
    meter_speed_avg_all = tnt.meter.AverageValueMeter()

    CsvUtils.create_local(args)

    str_params_table = []
    for arg in vars(args):
        key = arg
        value = getattr(args, arg)
        str_params_table.append('{}:\t{}'.format(key, value))
    str_params_table = '\n\n'.join(str_params_table)

    tensorboard_writer.add_text(tag='params', text_string=str_params_table)

    logging_utils.info('cuda devices: {}'.format(torch.cuda.device_count()))

    # read data dimensions
    for filename in os.listdir(args.path_value_maps):
        filename = args.path_value_maps + '/' + filename
        if os.path.isdir(filename) and os.path.exists(filename + '/metadata.json'):
            with open(filename + '/metadata.json', 'r') as fp:
                metadata = json.load(fp)
            args.data_size = tuple(metadata['memmap_shape'])
            break

    model = None

    if args.model:
        Model = getattr(__import__('modules.' + args.model, fromlist=['Model']), 'Model')
    model = Model(args)  # placeholder for autocomplete
    # for param in model.parameters():
    #   print(param.data)

    if args.data_loader:
        DataLoader = getattr(__import__('modules.' + args.data_loader, fromlist=['DataLoader']), 'DataLoader')
    data_loader = DataLoader() # placeholder for autocomplete

    # load datasets
    data_loader_train, data_loader_test = data_loader.get_data_loaders(args)

    # save model description (important for testing)
    with open(os.path.join(run_path + '/model_desc.json'), 'w') as outfile:
        json.dump(args.__dict__, outfile, indent=4)

    # multi GPU support
    is_data_parallel = False
    if torch.cuda.device_count() > 1:
        model = torch.nn.DataParallel(model, dim=0)
        is_data_parallel = True

    if args.is_cuda:
        model = model.cuda()

    model_parameters = filter(lambda it: it.requires_grad, model.parameters())

    # main training loop
    optimizer_func = None
    if args.optimizer == 'adam':
        optimizer_func = torch.optim.Adam(
            model_parameters,
            lr=args.learning_rate,
            weight_decay=args.reg_beta
        )
    elif args.optimizer == 'rmsprop':
        optimizer_func = torch.optim.RMSprop(
            model_parameters,
            lr=args.learning_rate,
            weight_decay=args.reg_beta
        )
    elif args.optimizer == 'sgd':
        optimizer_func = torch.optim.SGD(
            model_parameters,
            lr=args.learning_rate,
            momentum=args.sgd_momentum,
            weight_decay=args.reg_beta
        )

    # must use optimizer SGD
    scheduler = None
    if args.lr_scheduler == 'steplr':
        scheduler = torch.optim.lr_scheduler.StepLR(optimizer_func, step_size=args.lr_step_size, gamma=args.lr_gamma, last_epoch=-1)
    elif args.lr_scheduler == 'cosineannealinglr':
        scheduler = CosineWithRestarts(optimizer_func, T_max=3, last_epoch=-1)

    loss_func = torch.nn.MSELoss(size_average=(not args.is_loss_sum_batch))
    if args.loss == 'mse':
        loss_func = torch.nn.MSELoss(size_average=(not args.is_loss_sum_batch)) # summed MSE loss not average
    elif args.loss == 'msa':
        loss_func = torch.nn.L1Loss(size_average=(not args.is_loss_sum_batch))
    elif args.loss == 'hinge':
        loss_func = torch.nn.SmoothL1Loss(size_average=(not args.is_loss_sum_batch)) # hinge loss

    # RNN model with deltas
    def rollout_delta_output_map(output, batch):
        input_len = batch['len'] # (batch, len)
        x = batch['x'] # (seq, batch, features)
        deltas = output.data.numpy() # (seq, batch, features)

        x = np.swapaxes(x, 0, 1) # (batch, seq, features)
        deltas = np.swapaxes(deltas, 0, 1) # (batch, seq, features)

        batch_size = x.shape[0] # (batch, seq, chan, w, h)

        compiled_y = np.zeros(shape=(batch_size, x.shape[2], x.shape[3], x.shape[4]))
        for idx_batch in range(batch_size):
            compiled_y[idx_batch] = x[idx_batch][0] # first map
            for idx_step in range(input_len[idx_batch]): # apply deltas
                compiled_y[idx_batch] += deltas[idx_batch][idx_step]
        output_y = batch['y_real'][-1]
        return torch.Tensor(compiled_y).type(torch.FloatTensor), torch.Tensor(output_y).type(torch.FloatTensor)

    def get_model_forward(model, batch, hidden=None):

        input = torch.tensor(batch['x']).type(torch.FloatTensor) # input do not need grad
        input_start = torch.tensor(batch['x_start']).type(torch.FloatTensor)
        if args.is_cuda:
            input = input.cuda()
            input_start = input_start.cuda()
        inputs = [input]

        input_len = torch.tensor(batch['len']).type(torch.IntTensor)  # lengths of x & y
        if args.is_cuda:
            input_len = input_len.cuda()
        inputs.append(input_len)
        inputs.append(hidden)
        input_start = input_start.permute(1, 0, 2, 3, 4).contiguous()
        # (seq, batch, features) => (batch, seq, features)
        input = input.permute(1, 0, 2, 3, 4).contiguous()
        output, hidden = model.forward(input, input_start, input_len, hidden) # (batch, seq, features) , (batch, features)
        # (batch, seq, features) => (seq, batch, features)
        output = output.permute(1, 0, 2, 3, 4).contiguous()

        output_y = torch.tensor(batch['y']).type(torch.FloatTensor)

        if args.is_cuda:
            output = output.cpu()
        return output, output_y, inputs, hidden


    def generate_maps(epoch=0):
        count_test_map = 0
        idx_batch_offset = 0
        with torch.no_grad():
            for batch in data_loader_test:

                inputs = copy.copy(batch)

                # batch['x'] => (len, batch, features)

                inputs['x'] = np.array([inputs['x'][0]]) # only first step
                inputs['x_start'] = np.array([inputs['x_start'][0]])  # only first step
                # set last y as real last input, because len are different
                for i in range(inputs['y'].shape[1]):
                    inputs['y'][-1][i] = inputs['y'][inputs['len'][i] - 1][i]
                inputs['y'] = np.array([inputs['y'][-1]])  # only last step for comparison

                if 'y_real' in batch:  # delta mode
                    inputs['y'] = inputs['y_real']

                inputs_len = inputs['len']
                inputs['len'] = np.ones(inputs['len'].shape[0])

                inputs['x'] = torch.Tensor(inputs['x']).type(torch.FloatTensor) # for first iteration uniformity in code
                inputs['x_start'] = torch.Tensor(inputs['x_start']).type(torch.FloatTensor)  # for first iteration uniformity in code

                model_module = model
                if is_data_parallel:
                    model_module = model.module
                hidden = model_module.init_hidden(batch_size=inputs['len'].shape[0])

                for idx_batch in range(inputs['len'].shape[0]):
                    data_y = inputs['x'][0][idx_batch].detach().numpy()[0]  # select grayscale channel
                    tensorboard_utils.addPlot2D_color_goal_from_real(
                        dataXY=data_y,
                        dataReal=inputs['y'][0][idx_batch][0],
                        tag=f'increment_{idx_batch + idx_batch_offset}_epoch_{epoch}',
                        global_step=0
                    )

                max_iterations = int(inputs_len[0])
                for iter_count in range(max_iterations):
                    output, _, _, hidden = get_model_forward(model, inputs, hidden)

                    if 'y_real' in batch:  # delta mode
                        output = inputs['x'] + output

                    for idx_batch in range(inputs['len'].shape[0]):
                        data_y = output[0][idx_batch].detach().numpy()[0]  # select grayscale channel
                        tensorboard_utils.addPlot2D_color_goal_from_real(
                            dataXY=data_y,
                            dataReal=inputs['y'][0][idx_batch][0],
                            tag=f'increment_{idx_batch + idx_batch_offset}_epoch_{epoch}',
                            global_step=iter_count+1,
                        )

                    # will converge over all batch for precise convergence batch_size = 1 should be used
                    convergence_value = np.mean( np.abs(output.detach().numpy() - inputs['y']))
                    if convergence_value < args.rnn_convergence_limit:
                        break
                    inputs['x'] = output

                for idx_batch in range(inputs['len'].shape[0]):
                    if epoch == 0:
                        # target map does not change so draw only once
                        data_y = inputs['y'][0][idx_batch][0]  # select grayscale channel
                        tensorboard_utils.addPlot2D_color_goal(
                            dataXY=data_y,
                            tag=f'real_{idx_batch + idx_batch_offset}_',
                        )

                    data_y = output[0][idx_batch].detach().numpy()[0]  # select grayscale channel
                    tensorboard_utils.addPlot2D_color_goal_from_real(
                        dataXY=data_y,
                        dataReal=inputs['y'][0][idx_batch][0],
                        tag=f'output_{idx_batch + idx_batch_offset}_',
                        global_step=epoch,
                    )

                    count_test_map += 1

                if count_test_map >= args.tensorboard_maps_count:
                    break
                idx_batch_offset += inputs['len'].shape[0]
                torch.cuda.empty_cache()




    state = {
        'epoch': 0,
        'loss': -1,
        'best_loss': -1,
        'test_loss': -1,
        'success_rate': -1,
        'score': -1,
        'best_success_rate': -1,
        'best_score': -1,
        'avg_epoch_time': -1,
        'epoch_time': -1,
        'early_stopping_patience': 0,
        'early_percent_improvement': 0,
        'speed': -1,
        'avg_speed': -1
    }
    avg_time_epochs = []

    model = model.eval()
    generate_maps(epoch=0)
    model = model.train()

    model_trainable_params = sum(p.numel() for p in model.parameters() if p.requires_grad)
    model_all_params = sum(p.numel() for p in model.parameters())
    logging_utils.info('Model total parameters: {}'.format(model_all_params))
    logging_utils.info('trainable parameters: {}'.format(model_trainable_params))

    is_graph_saved = False

    def log_prameters(epoch=0):
        for name, param in model.named_parameters():
            # histogram bins estimators https://docs.scipy.org/doc/numpy-1.13.0/reference/generated/numpy.histogram.html
            tensorboard_writer.add_histogram('param_' + name, param.clone().cpu().data.numpy(), epoch, bins='doane')
    log_prameters(epoch=0)

    for epoch in range(1, args.epochs + 1):

        logging_utils.info('epoch: {} / {}'.format(epoch, args.epochs))

        meter_loss.reset()
        meter_test_loss.reset()
        meter_success_rate.reset()
        meter_score.reset()
        meter_speed.reset()

        time_epoch = time.time()

        # lr scheduler step
        if args.lr_scheduler == 'steplr':
            scheduler.step()

        state['epoch'] = epoch
        for batch in data_loader_train:

            optimizer_func.zero_grad()
            model.zero_grad()

            # RNN model
            model_module = model
            if is_data_parallel:
                model_module = model.module
            hidden = model_module.init_hidden(batch_size=batch['len'].shape[0])

            output, output_y, _, _ = get_model_forward(model, batch, hidden)

            loss = loss_func(output, output_y)

            if args.loss == 'mse_custom_goal':
                # help to emphasize goal point
                output_y_flat = output_y.view(output_y.size(0), -1) # flatten (batch, channel * w * h)
                output_flat = output.view(output_y.size(0), -1)  # flatten (batch, channel * w * h)

                idx_goal = torch.argmax(output_y_flat, dim=1) # real target state with max value

                # add offsets for 1d shape
                for idx in range(idx_goal.size(0)):
                    idx_goal[idx] += idx * output_flat.size(1)

                output_y_flat_1d = output_y_flat.view(output_y_flat.size(0) * output_y_flat.size(1))
                output_flat_1d = output_flat.view(output_flat.size(0) * output_flat.size(1))

                # loss_func is MSE
                output_y_goal = output_y_flat_1d[idx_goal]
                output_goal = output_flat_1d[idx_goal]

                loss = (1 - args.loss_custom_coef) * loss + args.loss_custom_coef * loss_func(output_goal, output_y_goal)

            loss.backward()

            # sometimes gradient clipping is beneficial, need testing
            #torch.nn.utils.clip_grad_norm(model.parameters(), max_norm=1.0)

            optimizer_func.step()

            loss_scalar = np.average(loss.data)
            meter_loss.add(loss_scalar)

            if args.is_quick_test:
                break
            torch.cuda.empty_cache()

        if args.lr_scheduler == 'cosineannealinglr':
            scheduler.step()
        log_prameters(epoch=epoch)
        model = model.eval()


        generate_maps(epoch=epoch)

        success_map_count = 0
        idx_batch_offset = 0
        with torch.no_grad():
            for batch in data_loader_test:

                # RNN model
                model_module = model
                if is_data_parallel:
                    model_module = model.module
                hidden = model_module.init_hidden(batch_size=batch['len'].shape[0])

                time_start = datetime.now()

                output, output_y, inputs, _ = get_model_forward(model, batch, hidden)

                timespan_sec = (datetime.now() - time_start).total_seconds() / args.batch_size
                meter_speed.add(timespan_sec)
                meter_speed_avg_all.add(timespan_sec)

                loss = loss_func.forward(output, output_y)
                loss = np.average(loss.data)

                # metrics
                if args.is_metric_success_rate or args.is_metric_score:

                    if 'y_real' in batch:
                        output, output_y = rollout_delta_output_map(output, batch)  # (batch, features)
                    else:
                        for i in range(batch['x'].shape[1]):  # for all samples
                            sample_len = batch['len'][i]
                            output[-1][i] = output[sample_len - 1][
                                i]  # store in last position the last timestep (for shorter seq move to end)
                            output_y[-1][i] = output_y[sample_len - 1][i]
                        output_y = output_y[-1]  # last timestep
                        output = output[-1]  # last timestep
                        # (batch, features)

                    metrics_time = time.time()
                    if epoch == 1:
                        score_real, success_rate_real, success_map_real = Metrics_success_score.metric_success_rate(output_y, output_y, args.metric_success_rate_threads)
                        meter_success_rate_real.add(success_rate_real)
                        meter_score_real.add(score_real)
                    score, success_rate, success_map = Metrics_success_score.metric_success_rate(output, output_y, args.metric_success_rate_threads)

                    metrics_time = (time.time() - metrics_time) / 60.0

                    if success_map_count < args.tensorboard_maps_count:
                        for idx, each in enumerate(output):
                            tensorboard_utils.addPlot2D_color_goal(
                                dataXY=success_map[idx],
                                tag=f'success_map_{idx + idx_batch_offset}_',
                                global_step=epoch,
                            )
                            if epoch == 1:
                                tensorboard_utils.addPlot2D_color_goal(
                                    dataXY=success_map_real[idx],
                                    tag=f'success_map_real_{idx + idx_batch_offset}_',
                                )
                            success_map_count += 1
                    idx_batch_offset += success_map.shape[0]

                if not is_graph_saved and args.is_add_graph_to_tensorboard:
                    is_graph_saved = True
                    inputs_single = []
                    for idx, each in enumerate(inputs):
                        dim_batch = 0
                        if idx == 0:
                            # for RNN first input has shape (timesteps, batch, features)
                            dim_batch = 1
                        each = each.index_select(dim=dim_batch, index=torch.tensor([0]))

                        # mode to use single timestep
                        if idx == 0:
                            each = each.index_select(dim=0, index=torch.tensor([0]))
                        else:
                            each[0].data[0] = 1

                        inputs_single.append(each)

                    # init hidden for 1 batch size
                    model.reset_hidden(1)

                    if args.is_save_onnx_graph:
                        file_onnx = f'{run_path}/graph.proto'
                        torch_onnx.export(model, tuple(inputs_single), file_onnx, verbose=True) # need latest pytorch
                        tensorboard_writer.add_graph_onnx(file_onnx) # conda install -c conda-forge onnx
                    else:
                        tensorboard_writer.add_graph(model, tuple(inputs_single), verbose=True) # https://github.com/danakianfar/tensorboard-pytorch/commit/3e5559a1358ed7efe54ec903236c640c7043dc96

                meter_test_loss.add(loss)
                if args.is_metric_success_rate:
                    meter_success_rate.add(success_rate)
                if args.is_metric_score:
                    meter_score.add(score)

                if args.is_quick_test:
                    break

        state_before = copy.deepcopy(state)

        model_module = model
        if is_data_parallel:
            model_module = model.module

        if state['best_loss'] < state['test_loss']:
            state['best_loss'] = state['test_loss']
            torch.save(model_module.state_dict(), os.path.join(run_path, 'best.pt'))

        torch.save(model_module.state_dict(), os.path.join(run_path, 'last.pt'))

        model = model.train()

        time_epoch = (time.time() - time_epoch) / 60.0
        percent = epoch / args.epochs
        eta = ((args.epochs - epoch) * time_epoch)

        state['speed'] = meter_speed.value()[0]
        state['avg_speed'] = meter_speed_avg_all.value()[0]
        state['loss'] = meter_loss.value()[0]
        state['test_loss'] = meter_test_loss.value()[0]
        state['success_rate'] = meter_success_rate.value()[0]
        state['score'] = meter_score.value()[0]
        state['epoch_time'] = time_epoch
        state['best_score'] = max(state['score'], state['best_score'])
        state['best_success_rate'] = max(state['success_rate'], state['best_success_rate'])

        if epoch == 1:
            state['best_loss'] = state['loss']
        else:
            state['best_loss'] = min(state['loss'], state['best_loss'])


        # early stopping
        if not args.early_stopping_param == 'success_rate':
            percent_improvement = -(state[args.early_stopping_param] - state_before[args.early_stopping_param]) / state_before[args.early_stopping_param]
        else:
            percent_improvement = (state[args.early_stopping_param] - state_before[args.early_stopping_param]) / state_before[args.early_stopping_param]
        if state[args.early_stopping_param] >= 0:
            if args.early_stopping_delta_percent > percent_improvement:
                state['early_stopping_patience'] += 1
            else:
                state['early_stopping_patience'] = 0
        state['early_percent_improvement'] = percent_improvement

        CsvUtils.add_results_local(args, state)
        CsvUtils.add_results(args, state)

        tensorboard_writer.add_scalar(tag='loss', scalar_value=state['loss'], global_step=epoch)
        tensorboard_writer.add_scalar(tag='test_loss', scalar_value=state['test_loss'], global_step=epoch)
        tensorboard_writer.add_scalar(tag='improvement', scalar_value=state['early_percent_improvement'], global_step=epoch)
        if args.is_metric_speed:
            tensorboard_writer.add_scalar(tag='speed', scalar_value=state['speed'],
                                          global_step=epoch)
            tensorboard_writer.add_scalar(tag='avg_speed', scalar_value=state['avg_speed'],
                                          global_step=epoch)
        if args.is_metric_success_rate:
            tensorboard_writer.add_scalar(tag='success_rate', scalar_value=state['success_rate'],
                                          global_step=epoch)
            tensorboard_writer.add_scalar(tag='success_rate_real', scalar_value=meter_success_rate_real.value()[0],
                                          global_step=epoch)
        if args.is_metric_score:
            tensorboard_writer.add_scalar(tag='score', scalar_value=state['score'],
                                          global_step=epoch)
            tensorboard_writer.add_scalar(tag='score_real', scalar_value=meter_score_real.value()[0],
                                          global_step=epoch)

        avg_time_epochs.append(time_epoch)
        state['avg_epoch_time'] = np.average(avg_time_epochs)

        logging_utils.info(
            '{}% each: {} min eta: {} min loss: {:10.2f} improve: {:10.2f}'
                .format(
                round(percent * 100, 2),
                round(time_epoch, 2),
                round(eta, 2),
                state['loss'],
                percent_improvement
            ))

        if state['early_stopping_patience'] >= args.early_stopping_patience or \
                (state['early_percent_improvement'] == 0 and state['epoch'] > 1):
            logging_utils.info('early stopping')
            break

        command = CommandTxtUtils()
        if command.is_stopped(args):
            logging_utils.info('command stop')

        if args.is_quick_test:
            break

    tensorboard_writer.close()

    CsvUtils.add_results(args, state)