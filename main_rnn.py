import os
import numpy as np
import time
import argparse
import json
import copy
import logging

import torch
import torch.nn
import torch.utils.data
from torch.autograd import Variable
import torch.onnx as torch_onnx
import tensorboardX
import torchnet as tnt

from datetime import datetime

from modules.file_utils import FileUtils
from modules.tensorboard_utils import TensorBoardUtils
from modules.logging_utils import LoggingUtils
from modules.args_utils import ArgsUtils
from modules.csv_utils import CsvUtils
from modules.command_txt_utils import CommandTxtUtils
from modules.metrics_success_score import  Metrics_success_score
from modules.lr_scheduler import CosineWithRestarts
from modules.not_used.model_cnn_simple import Model
from modules.data_loader_cnn_simple import DataLoader

if __name__ == '__main__': # avoid problems with spawned processes
    parser = argparse.ArgumentParser(description='Model trainer')

    parser.add_argument('-is_cuda', default=False, type=lambda x: (str(x).lower() == 'true'))

    parser.add_argument('-id', default=0, type=int)
    parser.add_argument('-repeat_id', default=0, type=int)
    parser.add_argument('-report', default='summary', type=str)
    parser.add_argument('-params_report', nargs='*', required=False) # extra params for report global for series of runs
    parser.add_argument('-params_report_local', nargs='*', required=False) # extra params for local run report

    parser.add_argument('-name', help='Run name, by default date', default='', type=str)

    parser.add_argument('-epochs', default=10, type=int)
    parser.add_argument('-model', default='model_cnn_simple', type=str)

    parser.add_argument('-data_loader', default='data_loader_cnn_rnn_simple', type=str)
    parser.add_argument('-is_data_normalization', default='basic', type=str)
    parser.add_argument('-is_debug_data_loader', default=False, type=lambda x: (str(x).lower() == 'true'))

    parser.add_argument('-path_value_maps', default='../VIN_Research_ValueIterationGridmap/valueMaps', type=str)

    parser.add_argument('-learning_rate', default=1e-4, type=float)
    parser.add_argument('-optimizer', default='adam', type=str)
    parser.add_argument('-sgd_momentum', default=0.9, type=float)
    parser.add_argument('-reg_beta', default=0, type=float)
    parser.add_argument('-is_loss_sum_batch', default=False, type=lambda x: (str(x).lower() == 'true'))
    parser.add_argument('-dropout', default=0.3, type=float)
    parser.add_argument('-lr_scheduler', default='', type=str)
    parser.add_argument('-lr_gamma', default=0.1, type=float)
    parser.add_argument('-lr_step_size', default=7, type=int)

    parser.add_argument('-loss', default='mse', help='mse msa hinge mse_custom_goal', type=str)
    parser.add_argument('-loss_custom_coef', default=0.1, type=float)

    parser.add_argument('-batch_size', default=64, type=int)

    parser.add_argument('-tensorboard_maps_count', default=10, type=int)

    parser.add_argument('-rnn_convergence_limit', default=0.01, type=float)

    parser.add_argument('-early_stopping_patience', default=3, type=int)
    parser.add_argument('-early_stopping_param', default='test_loss', type=str)
    parser.add_argument('-early_stopping_delta_percent', default=0.05, type=float)

    parser.add_argument('-is_add_graph_to_tensorboard', default=False, type=lambda x: (str(x).lower() == 'true'))
    parser.add_argument('-is_save_onnx_graph', default=False, type=lambda x: (str(x).lower() == 'true'))

    parser.add_argument('-is_unet', default=False, type=lambda x: (str(x).lower() == 'true'))
    parser.add_argument('-preload_weights_filename', default="none", type=str)

    parser.add_argument('-is_metric_success_rate', default=True, type=lambda x: (str(x).lower() == 'true'))
    parser.add_argument('-metric_success_rate_threads', default=8, type=int)
    parser.add_argument('-is_metric_score', default=False, type=lambda x: (str(x).lower() == 'true'))
    parser.add_argument('-is_metric_speed', default=True, type=lambda x: (str(x).lower() == 'true'))

    parser.add_argument('-is_quick_test', default=False, type=lambda x: (str(x).lower() == 'true')) # train/test only on a single batch

    parser.add_argument('-rnn_layer_count', default=1, type=int)
    parser.add_argument('-compression_fc', default=0.1, type=float)
    parser.add_argument('-init_values', default='new', type=str)

    parser.add_argument('-init_bias', default='zeros', type=str)
    parser.add_argument('-init_weights', default='xavier_normal', type=str)
    parser.add_argument('-init_bn', default='uniform', type=str)

    args, args_other = parser.parse_known_args()

    tmp = ['epoch', 'id', 'name', 'repeat_id', 'loss', 'test_loss', 'best_loss', 'avg_epoch_time']
    if args.is_metric_success_rate:
        tmp += ['success_rate', 'best_success_rate']
    if args.is_metric_score:
        tmp += ['score', 'best_score']
    if args.is_metric_speed:
        tmp += ['speed', 'avg_speed']
    if not args.params_report is None:
        for it in args.params_report:
            if not it in tmp:
                tmp.append(it)
    args.params_report = tmp

    tmp = ['epoch', 'loss', 'test_loss', 'epoch_time', 'early_percent_improvement']
    if args.is_metric_success_rate:
        tmp += ['success_rate']
    if args.is_metric_score:
        tmp += ['score']
    if args.is_metric_speed:
        tmp += ['speed']
    if not args.params_report_local is None:
        for it in args.params_report_local:
            if not it in tmp:
                tmp.append(it)
    args.params_report_local = tmp

    FileUtils.createDir('./reports')
    filename = os.path.join('reports', args.report) + '.csv'
    if not os.path.exists(filename):
        with open(filename, 'w') as outfile:
            FileUtils.lock_file(outfile)
            outfile.write(','.join(args.params_report) + '\n')
            FileUtils.unlock_file(outfile)

    if len(args.name) == 0:
        args.name = datetime.now().strftime('%y-%m-%d_%H-%M-%S')

    FileUtils.createDir('./runs/' + args.report)
    run_path = './runs/' + args.report + '/' + args.name
    if os.path.exists(run_path):
        FileUtils.deleteDir(run_path, is_delete_dir_path=True)
    FileUtils.createDir(run_path)

    if not torch.cuda.is_available():
        args.is_cuda = False

    tensorboard_writer = tensorboardX.SummaryWriter(log_dir=run_path)
    tensorboard_utils = TensorBoardUtils(tensorboard_writer)
    logging_utils = LoggingUtils(filename=os.path.join(run_path, 'log.txt'))
    ArgsUtils.log_args(args, 'main.py', logging_utils)

    CsvUtils.create_local(args)

    str_params_table = []
    for arg in vars(args):
        key = arg
        value = getattr(args, arg)
        str_params_table.append('{}:\t{}'.format(key, value))
    str_params_table = '\n\n'.join(str_params_table)

    tensorboard_writer.add_text(tag='params', text_string=str_params_table)

    logging_utils.info('cuda devices: {}'.format(torch.cuda.device_count()))

    Model = getattr(__import__('modules.' + args.model, fromlist=['Model']), 'Model')
    model = Model(args)

    DataLoader = getattr(__import__('modules.' + args.data_loader, fromlist=['DataLoader']), 'DataLoader')
    data_loader = DataLoader()

    # load datasets
    data_loader_train, data_loader_test = data_loader.get_data_loaders(args)

    # save model description (important for testing)
    with open(os.path.join(run_path + '/model_desc.json'), 'w') as outfile:
        json.dump(args.__dict__, outfile, indent=4)

    # multi GPU support
    is_data_parallel = False
    if args.is_cuda == True and torch.cuda.device_count() > 1:
        model = torch.nn.DataParallel(model, dim=0)
        is_data_parallel = True

    if args.is_cuda:
        model = model.cuda()
    else:
        model = model.cpu()

    # main training loop
    optimizer_func = None
    if args.optimizer == 'adam':
        optimizer_func = torch.optim.Adam(
            model.parameters(),
            lr=args.learning_rate,
            weight_decay=args.reg_beta
        )
    elif args.optimizer == 'rmsprop':
        optimizer_func = torch.optim.RMSprop(
            model.parameters(),
            lr=args.learning_rate,
            weight_decay=args.reg_beta
        )
    elif args.optimizer == 'sgd':
        optimizer_func = torch.optim.SGD(
            model.parameters(),
            lr=args.learning_rate,
            momentum=args.sgd_momentum,
            weight_decay=args.reg_beta
        )

    loss_func = torch.nn.MSELoss(size_average=(not args.is_loss_sum_batch))
    if args.loss == 'mse':
        loss_func = torch.nn.MSELoss(size_average=(not args.is_loss_sum_batch))  # summed MSE loss not average
    elif args.loss == 'bce':
        loss_func = torch.nn.BCELoss(size_average=(not args.is_loss_sum_batch))
    elif args.loss == 'msa':
        loss_func = torch.nn.L1Loss(size_average=(not args.is_loss_sum_batch))
    elif args.loss == 'hinge':
        loss_func = torch.nn.SmoothL1Loss(size_average=(not args.is_loss_sum_batch))  # hinge loss


    # used for output data
    def min_max_norm(input):
        for out in range(input.shape[1]):
            min_val = input[0][out].min()
            max_val = input[0][out].max()
            if min_val == max_val:
                input[0][out] = 0.5
            else:
                input[0][out] = (input[0][out] - min_val) / (max_val - min_val)

        return input

    def sum_norm(input):
        output = []
        for dim in range(input.shape[0]):
            sum = input[dim].sum().detach()
            if sum != 0:
                output.append(input[dim] / sum)
            else:
                output.append(input[dim])
        output = torch.stack(output)

        return output

    def forward(batch, hidden=None):
        input = torch.tensor(batch['x']).type(torch.FloatTensor) # input do not need grad
        input_len = torch.tensor(batch['len']).type(torch.IntTensor)  # lengths of x & y
        if args.is_cuda:
            input = input.cuda()
            input_len = input_len.cuda()
        inputs = [input]
        inputs.append(input_len)
        inputs.append(hidden)

        # (seq, batch, features) => (batch, seq, features)
        input = input.permute(1, 0, 2, 3, 4).contiguous()
        output, hidden = model.forward(input, input_len, hidden)  # (batch, seq, features) , (batch, features)
        # (batch, seq, features) => (seq, batch, features)
        output = output.permute(1, 0, 2, 3, 4).contiguous()
        if args.is_cuda:
            output = output.cpu()

        output_y = torch.tensor(batch['y']).type(torch.FloatTensor)

        return output, output_y, hidden


    state = {
        'epoch': 0,
        'train_loss': -1,
        'best_loss': -1,
        'test_loss': -1,
        'success_rate': -1,
        'score': -1,
        'best_success_rate': -1,
        'best_score': -1,
        'avg_epoch_time': -1,
        'epoch_time': -1,
        'early_stopping_patience': 0,
        'early_percent_improvement': 0,
        'speed': -1,
        'avg_speed': -1,
        'rnn_convergence_iterations': 0,
    }
    avg_time_epochs = []

    meters = dict(
        train_loss=tnt.meter.AverageValueMeter(),
        test_loss=tnt.meter.AverageValueMeter(),
        success_rate=tnt.meter.AverageValueMeter(),
        score=tnt.meter.AverageValueMeter(),
        success_rate_real=tnt.meter.AverageValueMeter(),
        score_real=tnt.meter.AverageValueMeter(),
        speed=tnt.meter.AverageValueMeter(),
        speed_avg_all=tnt.meter.AverageValueMeter(),
        rnn_convergence_iterations=tnt.meter.AverageValueMeter(),
    )


    for epoch in range(1, args.epochs + 1):
        state_before = copy.deepcopy(state)
        logging_utils.info('epoch: {} / {}'.format(epoch, args.epochs))

        for key in meters.keys():
            if not key == 'speed_avg_all':
                meters[key].reset()

        time_epoch = time.time()

        model = model.train()
        torch.set_grad_enabled(True)
        for batch in data_loader_train:
            optimizer_func.zero_grad()

            model_module = model
            if is_data_parallel:
                model_module = model.module
            hidden = model_module.init_hidden(batch_size=batch['len'].shape[0])

            output, output_y, _ = forward(batch, hidden)

            # uncomment to normalize to sum 1

            # if args.loss == 'bce':
            #     output_y_loss = []
            #     output_loss = []
            #     for idx in range(output.shape[0]):
            #         output_y_loss.append(sum_norm(output_y[idx]))
            #         output_loss.append(sum_norm(output[idx]))
            #     output_loss = torch.stack(output_loss)
            #     output_y_loss = torch.stack(output_y_loss)
            #     loss = loss_func(output_loss, output_y_loss)
            # else:
            #     loss = loss_func(output, output_y)
            loss = loss_func(output, output_y)

            loss.backward()

            optimizer_func.step()

            meters['train_loss'].add(np.average(loss.data))

            if epoch == 1:
                if 'y_real' in batch:
                    output_y = torch.Tensor(batch['y_real'][-1]).type(torch.FloatTensor)
                else:
                    for i in range(batch['x'].shape[1]):  # for all samples
                        sample_len = batch['len'][i]
                        output_y[-1][i] = output_y[sample_len - 1][i]
                    output_y = output_y[-1]  # last timestep
                # (batch, features)
                score_real, success_rate_real, success_map_real = Metrics_success_score.metric_success_rate(output_y, output_y, args.metric_success_rate_threads)
                meters['success_rate_real'].add(success_rate_real)
                meters['score_real'].add(score_real)

            torch.cuda.empty_cache()

        model = model.eval()
        torch.set_grad_enabled(False)
        count_test_map = 0
        count_increment_map = 0
        count_zero_maps = 0
        count_iteration_scalar = 0
        idx_batch_offset = 0
        for batch in data_loader_test:
            map_count_from_batch = np.zeros((args.batch_size, 1), dtype=bool)
            local_dir_for_maps = 'output_maps'
            inputs = copy.copy(batch)

            # batch['x'] => (len, batch, features)
            inputs['x'] = np.array([inputs['x'][0]])  # only first step
            # set last y as real last input, because len are different
            for i in range(inputs['y'].shape[1]):
                inputs['y'][-1][i] = inputs['y'][inputs['len'][i] - 1][i]
            inputs['y'] = np.array([inputs['y'][-1]])  # only last step for comparison

            if 'y_real' in batch:
                inputs['y'] = inputs['y_real']
            inputs['y'] = torch.Tensor(inputs['y']).type(
                torch.FloatTensor)

            inputs_len = inputs['len']
            inputs['len'] = np.ones(inputs['len'].shape[0])

            inputs['x'] = torch.Tensor(inputs['x']).type(
                torch.FloatTensor)  # for first iteration uniformity in code

            model_module = model
            if is_data_parallel:
                model_module = model.module
            hidden = model_module.init_hidden(batch_size=inputs['len'].shape[0])

            for idx_batch in range(inputs['len'].shape[0]):
                if count_zero_maps <= args.tensorboard_maps_count:
                    data_y = inputs['x'][0][idx_batch][0].detach().numpy()  # select grayscale channel
                    tensorboard_utils.addPlot2D_color_goal_from_real(
                        dataXY=data_y,
                        dataReal=inputs['y'][0][idx_batch][0].detach().numpy(),
                        tag=f'increment_{idx_batch + idx_batch_offset}_epoch_{epoch}',
                        global_step=0
                    )
                    count_zero_maps += 1

            max_iterations = int(inputs_len[0])
            iteration_counter = np.zeros((inputs['x'].shape[1]))
            for iter_count in range(max_iterations):
                time_start = datetime.now()
                output, _, hidden = forward(inputs, hidden)

                timespan_sec = (datetime.now() - time_start).total_seconds() / args.batch_size
                meters['speed'].add(timespan_sec)
                meters['speed_avg_all'].add(timespan_sec)

                if 'y_real' in batch:  # delta mode
                    output = inputs['x'] + output
                    output = min_max_norm(output)

                if iter_count == 0:
                    final_output = output

                # will get converge values for each map
                convergence_values = np.mean(np.abs(output.detach().numpy() - inputs['y'].detach().numpy()), axis=(2, 3, 4))
                converged_maps = convergence_values < args.rnn_convergence_limit
                for index, value in enumerate(converged_maps[converged_maps == False]):
                    if iter_count == 0:
                        if count_increment_map <= args.tensorboard_maps_count:
                            count_increment_map += 1
                            map_count_from_batch[index] = True

                    if map_count_from_batch[index] == True:
                        data_y = output[0][index][0].detach().numpy()  # select grayscale channel
                        tensorboard_utils.addPlot2D_color_goal_from_real(
                            dataXY=data_y,
                            dataReal=inputs['y'][0][index][0].detach().numpy(),
                            tag=f'increment_{index + idx_batch_offset}_epoch_{epoch}',
                            global_step=iter_count + 1,
                        )

                    final_output[0][index] = output[0][index]
                    iteration_counter[index] += 1
                if np.all(converged_maps):
                    break

                inputs['x'] = output

            # uncomment for sum 1 normalization

            # if args.loss == 'bce':
            #     final_output_loss = copy.deepcopy(final_output)
            #     inputs_loss = copy.deepcopy(inputs['y'])
            #     final_output_loss[0] = sum_norm(final_output_loss[0])
            #     inputs_loss[0] = sum_norm(inputs_loss[0])
            #
            #     loss = loss_func.forward(final_output_loss, inputs_loss)
            # else:
            #     loss = loss_func.forward(final_output, inputs['y'])

            loss = loss_func.forward(final_output, inputs['y'])

            meters['test_loss'].add(np.average(loss.data))

            meters['rnn_convergence_iterations'].add(np.mean(iteration_counter))
            for i in range(len(iteration_counter)):
                if count_iteration_scalar <= args.tensorboard_maps_count:
                    tensorboard_writer.add_scalar(tag=f'map_{i+ idx_batch_offset}_iterations',
                                                  scalar_value=iteration_counter[i],
                                                  global_step=epoch)
                    count_iteration_scalar += 1

            if args.is_metric_success_rate or args.is_metric_score:
                score, success_rate, success_map = Metrics_success_score.metric_success_rate(final_output[0], inputs['y'][0], args.metric_success_rate_threads)
                meters['success_rate'].add(success_rate)
                meters['score'].add(score)

            for idx_batch in range(inputs['len'].shape[0]):
                if count_test_map <= args.tensorboard_maps_count:
                    if epoch == 1:
                        # create dir to save maps
                        FileUtils.createDir(os.path.join(run_path, local_dir_for_maps))

                        # target map does not change so draw only once
                        data_y = inputs['y'][0][idx_batch][0].detach().numpy()  # select grayscale channel
                        tensorboard_utils.addPlot2D_color_goal(
                            dataXY=data_y,
                            tag=f'real_{idx_batch + idx_batch_offset}_',
                        )
                    data_y = final_output[0][idx_batch][0].detach().numpy()  # select grayscale channel

                    # save array to file
                    np.save(os.path.join(run_path, local_dir_for_maps,
                                         f'output_{idx_batch + idx_batch_offset}_epoch_{epoch}.npy'), data_y)

                    tensorboard_utils.addPlot2D_color_goal_from_real(
                        dataXY=data_y,
                        dataReal=inputs['y'][0][idx_batch][0].detach().numpy(),
                        tag=f'output_{idx_batch + idx_batch_offset}_',
                        global_step=epoch,
                    )

                    # metrics
                    if args.is_metric_success_rate or args.is_metric_score:
                        tensorboard_utils.addPlot2D_color_goal_from_real(
                            dataXY=success_map[idx_batch],
                            dataReal=inputs['y'][0][idx_batch][0].detach().numpy(),
                            tag=f'generate_success_map_{idx_batch + idx_batch_offset}_',
                            global_step=epoch,
                        )

                    count_test_map += 1
            idx_batch_offset += inputs['len'].shape[0]

        model_module = model
        if is_data_parallel:
            model_module = model.module

        # if state['best_loss'] < state['test_loss']:
        #     state['best_loss'] = state['test_loss']
        #     torch.save(model_module.state_dict(), os.path.join(run_path, 'best.pt'))

        # save best success rate
        if meters['success_rate'].value()[0] > state['best_success_rate']:
            torch.save(model_module.state_dict(), os.path.join(run_path, 'best.pt'))

        torch.save(model_module.state_dict(), os.path.join(run_path, 'last.pt'))

        time_epoch = (time.time() - time_epoch) / 60.0
        percent = epoch / args.epochs
        eta = ((args.epochs - epoch) * time_epoch)

        state['epoch'] = epoch
        state['speed'] = meters['speed'].value()[0]
        state['avg_speed'] = meters['speed_avg_all'].value()[0]
        state['train_loss'] = meters['train_loss'].value()[0]
        state['test_loss'] = meters['test_loss'].value()[0]
        state['success_rate'] = meters['success_rate'].value()[0]
        state['score'] = meters['score'].value()[0]
        state['epoch_time'] = time_epoch
        state['best_score'] = max(state['score'], state['best_score'])
        state['best_success_rate'] = max(state['success_rate'], state['best_success_rate'])
        state['rnn_convergence_iterations'] = meters['rnn_convergence_iterations'].value()[0]

        if epoch == 1:
            state['best_loss'] = state['test_loss']
        else:
            state['best_loss'] = min(state['test_loss'], state['best_loss'])

        # early stopping
        if not args.early_stopping_param == 'success_rate':
            percent_improvement = -(state[args.early_stopping_param] - state_before[args.early_stopping_param]) / state_before[args.early_stopping_param]
        else:
            percent_improvement = (state[args.early_stopping_param] - state_before[args.early_stopping_param]) / state_before[args.early_stopping_param]
        if state[args.early_stopping_param] >= 0:
            if args.early_stopping_delta_percent > percent_improvement:
                state['early_stopping_patience'] += 1
            else:
                state['early_stopping_patience'] = 0
        state['early_percent_improvement'] = percent_improvement

        CsvUtils.add_results_local(args, state)
        CsvUtils.add_results(args, state)

        tensorboard_writer.add_scalar(tag='train_loss', scalar_value=state['train_loss'], global_step=epoch)
        tensorboard_writer.add_scalar(tag='test_loss', scalar_value=state['test_loss'], global_step=epoch)
        tensorboard_writer.add_scalar(tag='improvement', scalar_value=state['early_percent_improvement'], global_step=epoch)
        tensorboard_writer.add_scalar(tag='rnn_iterations', scalar_value=state['rnn_convergence_iterations'], global_step=epoch)
        if args.is_metric_speed:
            tensorboard_writer.add_scalar(tag='speed', scalar_value=state['speed'], global_step=epoch)
            tensorboard_writer.add_scalar(tag='avg_speed', scalar_value=state['avg_speed'], global_step=epoch)
        if args.is_metric_success_rate:
            tensorboard_writer.add_scalar(tag='success_rate', scalar_value=state['success_rate'], global_step=epoch)
            tensorboard_writer.add_scalar(tag='success_rate_real', scalar_value=meters['success_rate_real'].value()[0], global_step=epoch)
        if args.is_metric_score:
            tensorboard_writer.add_scalar(tag='score', scalar_value=state['score'], global_step=epoch)
            tensorboard_writer.add_scalar(tag='score_real', scalar_value=meters['score_real'].value()[0], global_step=epoch)

        avg_time_epochs.append(time_epoch)
        state['avg_epoch_time'] = np.average(avg_time_epochs)

        logging_utils.info(
            '{}% each: {} min eta: {} min loss: {:10.2f} improve: {:10.2f}'
                .format(
                round(percent * 100, 2),
                round(time_epoch, 2),
                round(eta, 2),
                state['test_loss'],
                percent_improvement
            ))

        # if state['early_stopping_patience'] >= args.early_stopping_patience and state['epoch'] > 1:
        #     logging_utils.info('early stopping')
        #     break

        command = CommandTxtUtils()
        if command.is_stopped(args):
            logging_utils.info('command stop')

        if args.is_quick_test:
            break

    tensorboard_writer.close()

    CsvUtils.add_results(args, state)