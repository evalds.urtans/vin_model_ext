import torch.nn as nn
import torch.nn.functional as F


def deconv3x3(in_channels, out_channels, stride=1, padding=1):
    """3x3 deconvolution with padding"""
    return nn.ConvTranspose2d(in_channels, out_channels, kernel_size=3, stride=stride,
                     padding=padding, bias=False)


class DeconvBlock2D(nn.Module):
    bottleneck_resize_factor = 2

    def __init__(self, in_channels, out_channels, stride=1, padding=0):
        super(DeconvBlock2D, self).__init__()

        if stride != 1 or in_channels != out_channels:
            self.is_upsample = True
        else:
            self.is_upsample = False

        self.deconv1 = deconv3x3(in_channels, out_channels, stride, padding+1)
        self.bn1 = nn.BatchNorm2d(out_channels)
        self.relu = nn.ReLU(inplace=True)
        self.deconv2 = deconv3x3(out_channels, out_channels)
        self.bn2 = nn.BatchNorm2d(out_channels)

        if self.is_upsample:
            self.deconv_upsample = nn.ConvTranspose2d(in_channels, out_channels, kernel_size=1, stride=stride, padding=padding, bias=False)

    def forward(self, x):
        residual = x

        out = self.deconv1(x)
        out = self.relu(out)
        out = self.bn1(out)
        out = self.deconv2(out)

        if self.is_upsample:
            residual = self.deconv_upsample(residual)

        out += residual
        out = self.relu(out)
        out = self.bn2(out)

        return out

class DeconvBlock2Dmaxpool(nn.Module):
    bottleneck_resize_factor = 2

    def __init__(self, in_channels, out_channels, stride=1):
        super(DeconvBlock2Dmaxpool, self).__init__()

        if stride != 1 or in_channels != out_channels:
            self.is_upsample = True
        else:
            self.is_upsample = False

        if stride != 1:
            self.poolLayers = True
            self.poolDeconv1 = nn.ConvTranspose2d(in_channels, out_channels, kernel_size=1, stride=1, bias=False)
            self.maxpool1 = nn.MaxUnpool2d(kernel_size=3, stride=stride, padding=1)
        else:
            self.poolLayers = False
            self.deconv1 = deconv3x3(in_channels, out_channels, stride)

        self.bn1 = nn.BatchNorm2d(out_channels)
        self.relu = nn.ReLU(inplace=True)
        self.deconv2 = deconv3x3(out_channels, out_channels)
        self.bn2 = nn.BatchNorm2d(out_channels)

        if self.is_upsample:
            if stride != 1:
                self.poolDeconv2 = nn.ConvTranspose2d(in_channels, out_channels, kernel_size=1, stride=1, bias=False)
                self.maxpool2 = nn.MaxUnpool2d(kernel_size=1, stride=stride)
            else:
                self.deconv_upsample = nn.Conv2d(in_channels, out_channels, kernel_size=1, stride=stride, bias=False)

    def forward(self, x, ind1=None, ind2=None):
        residual = x

        if not self.poolLayers:
            out = self.deconv1(x)
        else:
            out = self.maxpool1(x, indices=ind2)
            out = self.poolDeconv1(out)

        out = self.relu(out)
        out = self.bn1(out)
        out = self.deconv2(out)

        if self.is_upsample:
            if not self.poolLayers:
                residual = self.deconv_upsample(residual)
            else:
                residual = self.maxpool2(residual, indices=ind1)
                residual = self.poolDeconv2(residual)

        out += residual
        out = self.relu(out)
        out = self.bn2(out)

        return out
