import os
import torch
import torch.nn
import torch.nn.functional as F
import torch.utils.data
import json
import logging
import sklearn
import sklearn.model_selection
import numpy as np
import traceback, sys
# conda install -c anaconda psutil
# import psutil

class Dataset(torch.utils.data.dataset.Dataset):
    def __init__(self, maps_list, args):

        super(Dataset, self).__init__()

        self.data = []
        self.args = args

        for filename in maps_list:
            try:
                with open(filename + '/metadata.json', 'r') as fp:
                    metadata = json.load(fp)

                last_iteration_id = metadata['memmap_shape'][0] - 1

                map_data = np.memmap(
                    filename + f'/all_maps.memmap',
                    mode='r',
                    dtype=np.float16,
                    shape=tuple(metadata['memmap_shape']))

                self.data.append({
                    'data': map_data,
                    'last_id': last_iteration_id
                })

            except Exception as e:
                logging.error(filename)
                logging.error(str(e))
                logging.error(f'len(self.data) = {len(self.data)}')
                exc_type, exc_value, exc_tb = sys.exc_info()
                logging.error(traceback.format_exception(exc_type, exc_value, exc_tb))

        logging.info('dataset size: {}'.format(len(self.data)))

    def __getitem__(self, index):
        result = {
            'x': np.expand_dims(self.data[index]['data'][0], axis=0).astype(np.float),
            'y': np.expand_dims(self.data[index]['data'][self.data[index]['last_id']], axis=0).astype(np.float)
        }

        vmax = np.amax(result['y'])
        vmin = np.amin(result['y'])
        vdelta = vmax - vmin

        # normalize 0 to 1 for ReLU
        # Softplus higher range 0 to 2
        if self.args.is_data_normalization == 'basic':
            # normalize 0 to 1 for ReLU
            result['y'] = ((result['y'] - vmin) / vdelta)
            result['x'] = ((result['x'] - vmin) / vdelta)
        elif self.args.is_data_normalization == 'zero_centred':
            # normalize -1 to 1
            result['y'] = 2 * ((result['y'] - vmin)/( vmax - vmin)) -1
            result['x'] = 2 * ((result['x'] - vmin)/( vmax - vmin)) -1

        return result


    def __len__(self):
        return len(self.data)


class DataLoader(object):

    @staticmethod
    def get_data_loaders(args):

        maps_list = []
        for filename in os.listdir(args.path_value_maps):
            filename = args.path_value_maps + '/' + filename
            if os.path.isdir(filename):
                maps_list.append(filename)

        maps_list_train, maps_list_test = sklearn.model_selection.train_test_split(
            maps_list,
            test_size=0.2,
            shuffle=False)

        dataset_train = Dataset(maps_list_train, args)
        dataset_test = Dataset(maps_list_test, args)

        logging.info('train dataset')
        data_loader_train = torch.utils.data.DataLoader(
            dataset_train,
            batch_size=args.batch_size,
            # num_workers=32,
            shuffle=True)

        logging.info('test dataset')
        data_loader_test = torch.utils.data.DataLoader(
            dataset_test,
            batch_size=args.batch_size,
            # num_workers=32,
            shuffle=False)

        return data_loader_train, data_loader_test
