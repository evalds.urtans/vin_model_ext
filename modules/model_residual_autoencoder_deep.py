import torch
import torch.nn
import torch.utils.data
import numpy as np
import logging
from modules.block_resnet_2d import BasicBlock2D
from modules.block_resnet_deconv2d import DeconvBlock2D
import modules.torch_utils as torch_utils

class Model(torch.nn.Module):
    def __init__(self, args):
        super(Model, self).__init__()
        self.args = args

        # input 32 x 32
        self.start_conv_1 = torch.nn.Conv2d(in_channels=1, out_channels=16, kernel_size=4, bias=False)
        # conv layer count = 1
        # 29x29x16
        self.block2d_1_1 = BasicBlock2D(in_channels=16, out_channels=16, stride=1)
        # conv layer count = 3
        # 29x29x16
        self.block2d_1_2 = BasicBlock2D(in_channels=16, out_channels=16, stride=1)
        # conv layer count = 5
        # 29x29x16
        self.block2d_1_3 = BasicBlock2D(in_channels=16, out_channels=32, stride=2)
        # conv layer count = 8
        # 15x15x32
        self.block2d_1_4 = BasicBlock2D(in_channels=32, out_channels=32, stride=1)
        # conv layer count = 10
        # 15x15x32
        self.block2d_1_5 = BasicBlock2D(in_channels=32, out_channels=32, stride=1)
        # conv layer count = 12
        # 15x15x32
        self.block2d_1_6 = BasicBlock2D(in_channels=32, out_channels=64, stride=2, padding=1)
        # conv layer count = 15
        # 9x9x64
        self.block2d_1_7 = BasicBlock2D(in_channels=64, out_channels=64, stride=1)
        # conv layer count = 17
        # 9x9x64
        self.block2d_1_8 = BasicBlock2D(in_channels=64, out_channels=64, stride=1)
        # conv layer count = 19
        # 9x9x64
        self.block2d_1_9 = BasicBlock2D(in_channels=64, out_channels=128, stride=2)
        # conv layer count = 22
        # 5x5x128

        self.size_output_layers_encoder = [128, 5, 5]
        self.size_output_layers_encoder_prod = int(np.prod(self.size_output_layers_encoder))

        # add as parameter
        self.compression_fc = self.args.compression_fc
        self.fc_hidden_size = int(self.size_output_layers_encoder_prod * self.compression_fc)

        self.layers_fc = torch.nn.Sequential(
            torch.nn.Linear(in_features=self.size_output_layers_encoder_prod, out_features=self.fc_hidden_size),
            torch.nn.ReLU(),
            torch.nn.Linear(in_features=self.fc_hidden_size, out_features=self.size_output_layers_encoder_prod),
            torch.nn.ReLU()
        )

        self.block_deconv2d_1 = DeconvBlock2D(in_channels=128, out_channels=64, stride=2)
        # 9x9x64
        self.block_deconv2d_2 = DeconvBlock2D(in_channels=64, out_channels=64, stride=1)
        # 9x9x64
        self.block_deconv2d_3 = DeconvBlock2D(in_channels=64, out_channels=64, stride=1)
        # 9x9x64
        self.block_deconv2d_4 = DeconvBlock2D(in_channels=64, out_channels=32, stride=2, padding=1)
        # 15x15x32
        self.block_deconv2d_5 = DeconvBlock2D(in_channels=32, out_channels=32, stride=1)
        # 15x15x32
        self.block_deconv2d_6 = DeconvBlock2D(in_channels=32, out_channels=32, stride=1)
        # 15x15x32
        self.block_deconv2d_7 = DeconvBlock2D(in_channels=32, out_channels=16, stride=2)
        # 29x29x16
        self.block_deconv2d_8 = DeconvBlock2D(in_channels=16, out_channels=16, stride=1)
        # 29x29x16
        self.block_deconv2d_9 = DeconvBlock2D(in_channels=16, out_channels=16, stride=1)
        # 29x29x16
        self.end_deconv = torch.nn.ConvTranspose2d(in_channels=16, out_channels=1, kernel_size=4, bias=False)
        # 32x32x1

        # # for concat
        # self.block_deconv2d_1 = DeconvBlock2D(in_channels=128, out_channels=64, stride=2)
        # # 9x9x64
        # self.block_deconv2d_2 = DeconvBlock2D(in_channels=128, out_channels=64, stride=1)
        # # 9x9x64
        # self.block_deconv2d_3 = DeconvBlock2D(in_channels=128, out_channels=64, stride=1)
        # # 9x9x64
        # self.block_deconv2d_4 = DeconvBlock2D(in_channels=128, out_channels=32, stride=2, padding=1)
        # # 15x15x32
        # self.block_deconv2d_5 = DeconvBlock2D(in_channels=64, out_channels=32, stride=1)
        # # 15x15x32
        # self.block_deconv2d_6 = DeconvBlock2D(in_channels=64, out_channels=32, stride=1)
        # # 15x15x32
        # self.block_deconv2d_7 = DeconvBlock2D(in_channels=64, out_channels=16, stride=2)
        # # 29x29x16
        # self.block_deconv2d_8 = DeconvBlock2D(in_channels=32, out_channels=16, stride=1)
        # # 29x29x16
        # self.block_deconv2d_9 = DeconvBlock2D(in_channels=32, out_channels=16, stride=1)
        # # 29x29x16
        # self.end_deconv = torch.nn.ConvTranspose2d(in_channels=32, out_channels=1, kernel_size=4, bias=False)
        # # 32x32x1

        if not self.args.preload_weights_filename == "none":
            self.load_state_dict(torch.load(self.args.preload_weights_filename))
            for name, param in self.named_parameters():
                if 'block2d' in name:
                    param.requires_grad = False
                elif 'start_conv' in name:
                    param.requires_grad = False
        torch_utils.init_parameters(self)


    def forward(self, value):

        residual_1 = self.start_conv_1(value)
        residual_2 = self.block2d_1_1(residual_1)
        residual_3 = self.block2d_1_2(residual_2)
        residual_4 = self.block2d_1_3(residual_3)
        residual_5 = self.block2d_1_4(residual_4)
        residual_6 = self.block2d_1_5(residual_5)
        residual_7 = self.block2d_1_6(residual_6)
        residual_8 = self.block2d_1_7(residual_7)
        residual_9 = self.block2d_1_8(residual_8)
        out = self.block2d_1_9(residual_9)

        # fully connected
        out = out.view(-1, self.size_output_layers_encoder_prod)
        out = self.layers_fc(out)
        out = out.view(
            -1,
            self.size_output_layers_encoder[0],
            self.size_output_layers_encoder[1],
            self.size_output_layers_encoder[2]
        )

        # UNet architecture(residual connections from encoder to decoder)
        if self.args.is_unet == True:
            # # concat
            # out = torch.cat((self.block_deconv2d_1(out), residual_9), dim=1)
            # out = torch.cat((self.block_deconv2d_2(out), residual_8), dim=1)
            # out = torch.cat((self.block_deconv2d_3(out), residual_7),dim=1)
            # out = torch.cat((self.block_deconv2d_4(out), residual_6),dim=1)
            # out = torch.cat((self.block_deconv2d_5(out), residual_5),dim=1)
            # out = torch.cat((self.block_deconv2d_6(out), residual_4),dim=1)
            # out = torch.cat((self.block_deconv2d_7(out), residual_3),dim=1)
            # out = torch.cat((self.block_deconv2d_8(out), residual_2),dim=1)
            # out = torch.cat((self.block_deconv2d_9(out), residual_1),dim=1)

            # add
            out = self.block_deconv2d_1(out) + residual_9
            out = self.block_deconv2d_2(out) + residual_8
            out = self.block_deconv2d_3(out) + residual_7
            out = self.block_deconv2d_4(out) + residual_6
            out = self.block_deconv2d_5(out) + residual_5
            out = self.block_deconv2d_6(out) + residual_4
            out = self.block_deconv2d_7(out) + residual_3
            out = self.block_deconv2d_8(out) + residual_2
            out = self.block_deconv2d_9(out) + residual_1
            out = self.end_deconv(out)
        else:
            out = self.block_deconv2d_1(out)
            out = self.block_deconv2d_2(out)
            out = self.block_deconv2d_3(out)
            out = self.block_deconv2d_4(out)
            out = self.block_deconv2d_5(out)
            out = self.block_deconv2d_6(out)
            out = self.block_deconv2d_7(out)
            out = self.block_deconv2d_8(out)
            out = self.block_deconv2d_9(out)
            out = self.end_deconv(out)

        return out