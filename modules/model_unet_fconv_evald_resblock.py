import torch
import torch.nn
import torch.utils.data
import numpy as np
import logging
import torch.nn.functional as F
from modules.block_resnet_2d_std2 import ResBlock2D, DeResBlock2D
import modules.torch_utils as torch_utils


class Model(torch.nn.Module):
    def __init__(self, args):
        super(Model, self).__init__()
        self.args = args

        # input 32 x 32
        self.start_conv_1 = torch.nn.Conv2d(in_channels=1, out_channels=16, kernel_size=3, padding=1, bias=False)
        self.block2d_1_1 = ResBlock2D(in_channels=16, out_channels=16, is_conv_bias=False, kernel_size=3, stride=1)
        self.block2d_1_2 = ResBlock2D(in_channels=16, out_channels=32, is_conv_bias=False, kernel_size=4, stride=2)
        self.block2d_1_3 = ResBlock2D(in_channels=32, out_channels=32, is_conv_bias=False, kernel_size=3, stride=1)
        self.block2d_1_4 = ResBlock2D(in_channels=32, out_channels=64, is_conv_bias=False, kernel_size=4, stride=2)
        self.block2d_1_5 = ResBlock2D(in_channels=64, out_channels=64, is_conv_bias=False, kernel_size=3, stride=1)
        self.block2d_1_6 = ResBlock2D(in_channels=64, out_channels=128, is_conv_bias=False, kernel_size=4, stride=2)
        self.block2d_1_7 = ResBlock2D(in_channels=128, out_channels=128, is_conv_bias=False, kernel_size=4, stride=2)

        self.block_deconv2d_1 = DeResBlock2D(in_channels=128, out_channels=128, is_conv_bias=False, kernel_size=4, stride=2)
        self.block_deconv2d_2 = DeResBlock2D(in_channels=128, out_channels=64, is_conv_bias=False, kernel_size=4, stride=2)
        self.block_deconv2d_3 = DeResBlock2D(in_channels=64, out_channels=64, is_conv_bias=False, kernel_size=3, stride=1)
        self.block_deconv2d_4 = DeResBlock2D(in_channels=64, out_channels=32, is_conv_bias=False, kernel_size=4, stride=2)
        self.block_deconv2d_5 = DeResBlock2D(in_channels=32, out_channels=32, is_conv_bias=False, kernel_size=3, stride=1)
        self.block_deconv2d_6 = DeResBlock2D(in_channels=32, out_channels=16, is_conv_bias=False, kernel_size=4, stride=2)
        self.block_deconv2d_7 = DeResBlock2D(in_channels=16, out_channels=16, is_conv_bias=False, kernel_size=3, stride=1)
        self.end_deconv = torch.nn.ConvTranspose2d(in_channels=16, out_channels=1, kernel_size=3, padding=1, bias=False)
        # 32x32x1

        if not self.args.preload_weights_filename == "none":
            self.load_state_dict(torch.load(self.args.preload_weights_filename))
            for name, param in self.named_parameters():
                if 'block2d' in name:
                    param.requires_grad = False
                elif 'start_conv' in name:
                    param.requires_grad = False
        torch_utils.init_parameters(self)


    def forward(self, value):

        residual_1 = self.start_conv_1(value)
        residual_2 = self.block2d_1_1(residual_1)
        residual_3 = self.block2d_1_2(residual_2)
        residual_4 = self.block2d_1_3(residual_3)
        residual_5 = self.block2d_1_4(residual_4)
        residual_6 = self.block2d_1_5(residual_5)
        residual_7 = self.block2d_1_6(residual_6)
        value = self.block2d_1_7(residual_7)

        value = self.block_deconv2d_1(value) + residual_7
        value = self.block_deconv2d_2(value) + residual_6
        value = self.block_deconv2d_3(value) + residual_5
        value = self.block_deconv2d_4(value) + residual_4
        value = self.block_deconv2d_5(value) + residual_3
        value = self.block_deconv2d_6(value) + residual_2
        value = self.block_deconv2d_7(value) + residual_1
        value = torch.sigmoid(self.end_deconv(value))

        return value
