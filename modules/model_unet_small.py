import torch
import torch.nn
import torch.utils.data
import numpy as np
import logging
import torch.nn.functional as F
from modules.block_resnet_2d import BasicBlock2D
from modules.block_resnet_deconv2d import DeconvBlock2D
import modules.torch_utils as torch_utils

class Model(torch.nn.Module):
    def __init__(self, args):
        super(Model, self).__init__()
        self.args = args

        # input 32 x 32
        self.start_conv_1 = torch.nn.Conv2d(in_channels=1, out_channels=16, kernel_size=4, bias=False)
        # conv layer count = 1
        # 29x29x16
        self.block2d_1_1 = BasicBlock2D(in_channels=16, out_channels=16, stride=1)
        # conv layer count = 3
        # 29x29x16
        self.block2d_1_2 = BasicBlock2D(in_channels=16, out_channels=32, stride=2)
        # conv layer count = 8
        # 15x15x32
        self.block2d_1_3 = BasicBlock2D(in_channels=32, out_channels=32, stride=1)
        # conv layer count = 12
        # 15x15x32
        self.block2d_1_4 = BasicBlock2D(in_channels=32, out_channels=64, stride=2, padding=1)
        # conv layer count = 15
        # 9x9x64
        self.block2d_1_5 = BasicBlock2D(in_channels=64, out_channels=64, stride=1)
        # conv layer count = 19
        # 9x9x64
        self.block2d_1_6 = BasicBlock2D(in_channels=64, out_channels=128, stride=2)
        # conv layer count = 22
        # 5x5x128
        self.block2d_1_7 = BasicBlock2D(in_channels=128, out_channels=128, stride=2)
        # conv layer count = 25
        # 3x3x128

        self.size_output_layers_encoder = [128, 3, 3]
        self.size_output_layers_encoder_prod = int(np.prod(self.size_output_layers_encoder))
        # add as parameter
        self.compression_fc = self.args.compression_fc
        self.fc_hidden_size = int(self.size_output_layers_encoder_prod * self.compression_fc)

        self.layers_fc = torch.nn.Sequential(
            torch.nn.Linear(in_features=self.size_output_layers_encoder_prod, out_features=self.fc_hidden_size),
            torch.nn.ReLU(),
            torch.nn.Linear(in_features=self.fc_hidden_size, out_features=self.size_output_layers_encoder_prod),
            torch.nn.ReLU()
        )

        self.block_deconv2d_1 = DeconvBlock2D(in_channels=128, out_channels=128, stride=2)
        # 5x5x128
        self.block_deconv2d_2 = DeconvBlock2D(in_channels=128, out_channels=64, stride=2)
        # 9x9x64
        self.block_deconv2d_3 = DeconvBlock2D(in_channels=64, out_channels=64, stride=1)
        # 9x9x64
        self.block_deconv2d_4 = DeconvBlock2D(in_channels=64, out_channels=32, stride=2, padding=1)
        # 15x15x32
        self.block_deconv2d_5 = DeconvBlock2D(in_channels=32, out_channels=32, stride=1)
        # 15x15x32
        self.block_deconv2d_6 = DeconvBlock2D(in_channels=32, out_channels=16, stride=2)
        # 29x29x16
        self.block_deconv2d_7 = DeconvBlock2D(in_channels=16, out_channels=16, stride=1)
        # 29x29x16
        self.end_deconv = torch.nn.ConvTranspose2d(in_channels=16, out_channels=1, kernel_size=4, bias=False)
        # 32x32x1

        if not self.args.preload_weights_filename == "none":
            self.load_state_dict(torch.load(self.args.preload_weights_filename))
            for name, param in self.named_parameters():
                if 'block2d' in name:
                    param.requires_grad = False
                elif 'start_conv' in name:
                    param.requires_grad = False
        torch_utils.init_parameters(self)


    def forward(self, value):

        residual_1 = self.start_conv_1(value)
        residual_2 = self.block2d_1_1(residual_1)
        residual_3 = self.block2d_1_2(residual_2)
        residual_4 = self.block2d_1_3(residual_3)
        residual_5 = self.block2d_1_4(residual_4)
        residual_6 = self.block2d_1_5(residual_5)
        residual_7 = self.block2d_1_6(residual_6)
        value = self.block2d_1_7(residual_7)

        # fully connected
        value = value.view(-1, self.size_output_layers_encoder_prod)
        value = self.layers_fc(value)
        value = value.view(
            -1,
            self.size_output_layers_encoder[0],
            self.size_output_layers_encoder[1],
            self.size_output_layers_encoder[2]
        )

        value = self.block_deconv2d_1(value) + residual_7
        value = self.block_deconv2d_2(value) + residual_6
        value = self.block_deconv2d_3(value) + residual_5
        value = self.block_deconv2d_4(value) + residual_4
        value = self.block_deconv2d_5(value) + residual_3
        value = self.block_deconv2d_6(value) + residual_2
        value = self.block_deconv2d_7(value) + residual_1
        value = F.sigmoid(self.end_deconv(value))

        return value