import torch
import torch.nn
import torch.utils.data
import numpy as np
import modules.torch_utils as torch_utils
from torch.autograd import Variable
from modules.block_resnet_2d import BasicBlock2D
from modules.block_resnet_deconv2d import DeconvBlock2D


class Model(torch.nn.Module):
    def __init__(self, args):
        super(Model, self).__init__()
        self.args = args

        # input 32 x 32
        self.start_conv = torch.nn.Conv2d(in_channels=1, out_channels=32, kernel_size=8, bias=False)
        # 25x25x32
        self.block2d_1 = BasicBlock2D(in_channels=32, out_channels=32, stride=1)
        # 25x25x32
        self.block2d_2 = BasicBlock2D(in_channels=32, out_channels=64, stride=2)
        # 13x13x64
        self.block2d_3 = BasicBlock2D(in_channels=64, out_channels=64, stride=1)
        # 13x13x64
        self.block2d_4 = BasicBlock2D(in_channels=64, out_channels=128, stride=2)
        # 7x7x128

        self.size_output_layers_encoder = [128, 7, 7]
        self.size_output_layers_encoder_prod = int(np.prod(self.size_output_layers_encoder))

        self.compression_fc = self.args.compression_fc
        self.fc_hidden_size = int(self.size_output_layers_encoder_prod * self.compression_fc)

        self.layers_fc = torch.nn.Sequential(
            torch.nn.Linear(in_features=self.size_output_layers_encoder_prod, out_features=self.fc_hidden_size),
            torch.nn.ReLU()
        )

        self.len_layers_rnn = self.args.rnn_layer_count
        self.layer_rnn = torch.nn.GRU(
            input_size=self.fc_hidden_size,
            hidden_size=self.fc_hidden_size,
            num_layers=self.len_layers_rnn,
            batch_first=False
        )

        self.layers_fc_2 = torch.nn.Sequential(
            torch.nn.Linear(in_features=self.fc_hidden_size, out_features=self.size_output_layers_encoder_prod),
            torch.nn.ReLU()
        )

        self.block_deconv2d_1 = DeconvBlock2D(in_channels=128, out_channels=64, stride=2)
        # 13x13x64
        self.block_deconv2d_2 = DeconvBlock2D(in_channels=64, out_channels=64, stride=1)
        # 13x13x64
        self.block_deconv2d_3 = DeconvBlock2D(in_channels=64, out_channels=32, stride=2)
        # 25x25x32
        self.block_deconv2d_4 = DeconvBlock2D(in_channels=32, out_channels=32, stride=1)
        # 25x25x32
        self.end_deconv = torch.nn.ConvTranspose2d(in_channels=32, out_channels=1, kernel_size=8, bias=False)
        # 32x32x1

        torch_utils.init_parameters(self)

    def init_hidden(self, batch_size):

        # for dataparallel (batch_size, num_layers, hidden_dim)
        # but before input to RNN must have to be (num_layers, batch_size, hidden_dim)
        hidden_rnn = torch.zeros(batch_size, self.len_layers_rnn, self.fc_hidden_size)

        if self.args.is_cuda:
            hidden_rnn = hidden_rnn.cuda()

        return hidden_rnn # (batch_size, num_layers, hidden_dim)

    def forward(self, input_x, input_len, hidden):

        # (batch_size, seq, features) => (seq, batch_size, hidden_dim)

        hidden_perm = hidden.permute(1, 0, 2).contiguous()

        # optimization for parallel execution
        shape_input = input_x.size()
        len_max_seq = shape_input[1]
        len_batch = shape_input[0]

        # combine batch with seq for parallel processing through encoder
        # (seq_padded * batch, rgb, w, h)
        value = input_x.reshape((len_max_seq * len_batch, shape_input[2], shape_input[3], shape_input[4]))

        indexes_values = []
        for idx, each_seq_len in enumerate(input_len): # size of len_batch
            each_seq_len = each_seq_len.type(torch.LongTensor)
            index_offset = len_max_seq * idx # offset of batch
            indexes_values += np.arange(index_offset, index_offset+each_seq_len).tolist() # indexes index_offset ... index_offset+each_seq_len => [0, 1, 2, 3,]
        indexes_values = torch.tensor(indexes_values)
        if self.args.is_cuda:
            indexes_values = indexes_values.cuda()
        value = value.index_select(dim=0, index=indexes_values) # reduce data passed through CNN to only sequences without padding

        # (batch_size * seq, features)
        # encoder
        value = self.start_conv(value)
        value = self.block2d_1(value)
        value = self.block2d_2(value)
        value = self.block2d_3(value)
        value = self.block2d_4(value)
        value = value.view(-1, self.size_output_layers_encoder_prod)
        value = self.layers_fc(value)

        shape_after_encoder = value.size()
        # seq * batch, hidden_after_fc
        value_seq = torch.zeros((len_max_seq * len_batch, shape_after_encoder[1]))
        if self.args.is_cuda:
            value_seq = value_seq.cuda()
        for idx, idx_seq in enumerate(indexes_values):
            value_seq[idx_seq] = value[idx]

        #(batch_size, seq, features)
        value = value_seq.reshape((len_batch, len_max_seq, shape_after_encoder[1]))

        #(batch, seq, features) => (seq, batch, features)
        value = value.permute(1, 0, 2).contiguous()

        # rnn
        packed = torch.nn.utils.rnn.pack_padded_sequence(value, input_len, batch_first=False) # CUDNN packing
        value_packed, hidden_perm = self.layer_rnn(packed, hidden_perm)
        value, _ = torch.nn.utils.rnn.pad_packed_sequence(value_packed, batch_first=False) # CUDNN packing
        shape_after_rnn = value.size()

        # (seq, batch, features) => (batch, seq, features)
        value = value.permute(1, 0, 2).contiguous()
        value = value.reshape((len_max_seq * len_batch, shape_after_rnn[2]))

        value = value.index_select(dim=0, index=indexes_values)  # reduce data passed through CNN to only sequences without padding

        # decoder
        value = self.layers_fc_2(value)
        value = value.view(
            -1,
            self.size_output_layers_encoder[0],
            self.size_output_layers_encoder[1],
            self.size_output_layers_encoder[2]
        )
        value = self.block_deconv2d_1(value)
        value = self.block_deconv2d_2(value)
        value = self.block_deconv2d_3(value)
        value = self.block_deconv2d_4(value)
        value = self.end_deconv(value)
        shape_after_decoder = value.size()

        # unpack decoder
        value_seq = torch.zeros((len_batch * len_max_seq, shape_after_decoder[1], shape_after_decoder[2], shape_after_decoder[3]))
        if self.args.is_cuda:
            value_seq = value_seq.cuda()
        for idx, idx_seq in enumerate(indexes_values):
            value_seq[idx_seq] = value[idx]

        # batch, seq, channel, w, h
        value = value_seq.reshape((len_batch, len_max_seq, shape_after_decoder[1], shape_after_decoder[2], shape_after_decoder[3]))

        hidden_out = hidden_perm.permute(1, 0, 2).contiguous()

        return value, hidden_out