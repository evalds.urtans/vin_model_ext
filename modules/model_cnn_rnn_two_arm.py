import torch
import torch.nn
import torch.utils.data
import numpy as np
import modules.torch_utils as torch_utils
from torch.autograd import Variable
from modules.block_resnet_2d import BasicBlock2D
from modules.block_resnet_deconv2d import DeconvBlock2D


class Model(torch.nn.Module):
    def __init__(self, args):
        super(Model, self).__init__()
        self.args = args

        # for first map
        # input 32 x 32
        self.start_conv_1 = torch.nn.Conv2d(in_channels=1, out_channels=16, kernel_size=4, bias=False)
        # conv layer count = 1
        # 29x29x16
        self.block2d_1_1 = BasicBlock2D(in_channels=16, out_channels=16, stride=1)
        # conv layer count = 3
        # 29x29x16
        self.block2d_1_2 = BasicBlock2D(in_channels=16, out_channels=16, stride=1)
        # conv layer count = 5
        # 29x29x16
        self.block2d_1_3 = BasicBlock2D(in_channels=16, out_channels=32, stride=2)
        # conv layer count = 8
        # 15x15x32
        self.block2d_1_4 = BasicBlock2D(in_channels=32, out_channels=32, stride=1)
        # conv layer count = 10
        # 15x15x32
        self.block2d_1_5 = BasicBlock2D(in_channels=32, out_channels=32, stride=1)
        # conv layer count = 12
        # 15x15x32
        self.block2d_1_6 = BasicBlock2D(in_channels=32, out_channels=64, stride=2, padding=1)
        # conv layer count = 15
        # 9x9x64
        self.block2d_1_7 = BasicBlock2D(in_channels=64, out_channels=64, stride=1)
        # conv layer count = 17
        # 9x9x64
        self.block2d_1_8 = BasicBlock2D(in_channels=64, out_channels=64, stride=1)
        # conv layer count = 19
        # 9x9x64
        self.block2d_1_9 = BasicBlock2D(in_channels=64, out_channels=128, stride=2)
        # conv layer count = 22
        # 5x5x128

        # for delta maps
        # input 32 x 32
        self.start_conv_2 = torch.nn.Conv2d(in_channels=1, out_channels=16, kernel_size=4, bias=False)
        # conv layer count = 1
        # 29x29x16
        self.block2d_2_1 = BasicBlock2D(in_channels=16, out_channels=16, stride=1)
        # conv layer count = 3
        # 29x29x16
        self.block2d_2_2 = BasicBlock2D(in_channels=16, out_channels=16, stride=1)
        # conv layer count = 5
        # 29x29x16
        self.block2d_2_3 = BasicBlock2D(in_channels=16, out_channels=32, stride=2)
        # conv layer count = 8
        # 15x15x32
        self.block2d_2_4 = BasicBlock2D(in_channels=32, out_channels=32, stride=1)
        # conv layer count = 10
        # 15x15x32
        self.block2d_2_5 = BasicBlock2D(in_channels=32, out_channels=32, stride=1)
        # conv layer count = 12
        # 15x15x32
        self.block2d_2_6 = BasicBlock2D(in_channels=32, out_channels=64, stride=2, padding=1)
        # conv layer count = 15
        # 9x9x64
        self.block2d_2_7 = BasicBlock2D(in_channels=64, out_channels=64, stride=1)
        # conv layer count = 17
        # 9x9x64
        self.block2d_2_8 = BasicBlock2D(in_channels=64, out_channels=64, stride=1)
        # conv layer count = 19
        # 9x9x64
        self.block2d_2_9 = BasicBlock2D(in_channels=64, out_channels=128, stride=2)
        # conv layer count = 22
        # 5x5x128

        self.size_output_layers_encoder = [128, 5, 5]
        self.size_output_layers_encoder_prod = int(np.prod(self.size_output_layers_encoder))

        self.compression_fc = self.args.compression_fc
        self.fc_hidden_size = int(self.size_output_layers_encoder_prod * self.compression_fc)

        self.layers_fully_connected = torch.nn.Sequential(
            torch.nn.Linear(in_features=self.size_output_layers_encoder_prod, out_features=self.fc_hidden_size),
            torch.nn.ReLU()
        )

        self.len_layers_rnn = self.args.rnn_layer_count
        self.layer_rnn = torch.nn.GRU(
            input_size=self.fc_hidden_size,
            hidden_size=self.fc_hidden_size,
            num_layers=self.len_layers_rnn,
            batch_first=False
        )

        # test use half of concat for decoder
        self.size_input_layers_decoder = [128, 5, 5]
        self.size_input_layers_decoder_prod = int(np.prod(self.size_input_layers_decoder))
        self.layers_fully_connected_2 = torch.nn.Sequential(
            torch.nn.Linear(in_features=self.fc_hidden_size, out_features=self.size_input_layers_decoder_prod),
            torch.nn.ReLU()
        )

        self.block_deconv2d_1 = DeconvBlock2D(in_channels=128, out_channels=64, stride=2)
        # 9x9x64
        self.block_deconv2d_2 = DeconvBlock2D(in_channels=64, out_channels=64, stride=1)
        # 9x9x64
        self.block_deconv2d_3 = DeconvBlock2D(in_channels=64, out_channels=64, stride=1)
        # 9x9x64
        self.block_deconv2d_4 = DeconvBlock2D(in_channels=64, out_channels=32, stride=2, padding=1)
        # 15x15x32
        self.block_deconv2d_5 = DeconvBlock2D(in_channels=32, out_channels=32, stride=1)
        # 15x15x32
        self.block_deconv2d_6 = DeconvBlock2D(in_channels=32, out_channels=32, stride=1)
        # 15x15x32
        self.block_deconv2d_7 = DeconvBlock2D(in_channels=32, out_channels=16, stride=2)
        # 29x29x16
        self.block_deconv2d_8 = DeconvBlock2D(in_channels=16, out_channels=16, stride=1)
        # 29x29x16
        self.block_deconv2d_9 = DeconvBlock2D(in_channels=16, out_channels=16, stride=1)
        # 29x29x16
        self.end_deconv = torch.nn.ConvTranspose2d(in_channels=16, out_channels=1, kernel_size=4, bias=False)
        # 32x32x1


        if not self.args.preload_weights_filename == "none":
            my_state = self.state_dict()
            for name_load, param_load in torch.load(self.args.preload_weights_filename).items():
                for name, param in self.named_parameters():
                    if name == name_load:
                        if 'block2d_1' in name or 'start_conv_1' in name:
                            my_state[name].copy_(param_load.data)
                            param.requires_grad = False

        torch_utils.init_parameters(self)

    def init_hidden(self, batch_size):

        # for dataparallel (batch_size, num_layers, hidden_dim)
        # but before input to RNN must have to be (num_layers, batch_size, hidden_dim)
        hidden_rnn = torch.zeros(batch_size, self.len_layers_rnn, self.fc_hidden_size)

        if self.args.is_cuda:
            hidden_rnn = hidden_rnn.cuda()

        return hidden_rnn # (batch_size, num_layers, hidden_dim)

    def forward(self, input_x, input_start, input_len, hidden):

        # (batch_size, seq, features) => (num_layers, batch_size, hidden_dim)

        hidden_perm = hidden.permute(1, 0, 2).contiguous()

        # optimization for parallel execution
        shape_input_x = input_x.size()
        len_max_seq = shape_input_x[1]
        len_batch = shape_input_x[0]
        shape_input_start = input_start.size()

        # combine batch with seq for parallel processing through encoder
        # (seq_padded * batch, rgb, w, h)
        value_1 = input_start.reshape((len_max_seq * len_batch, shape_input_start[2], shape_input_start[3], shape_input_start[4]))
        value_2 = input_x.reshape((len_max_seq * len_batch, shape_input_x[2], shape_input_x[3], shape_input_x[4]))

        indexes_values = []
        for idx, each_seq_len in enumerate(input_len): # size of len_batch
            each_seq_len = each_seq_len.type(torch.LongTensor)
            index_offset = len_max_seq * idx # offset of batch
            indexes_values += np.arange(index_offset, index_offset+each_seq_len).tolist() # indexes index_offset ... index_offset+each_seq_len => [0, 1, 2, 3,]
        indexes_values = torch.tensor(indexes_values)
        if self.args.is_cuda:
            indexes_values = indexes_values.cuda()
        value_1 = value_1.index_select(dim=0, index=indexes_values) # reduce data passed through CNN to only sequences without padding
        value_2 = value_2.index_select(dim=0, index=indexes_values) # reduce data passed through CNN to only sequences without padding

        # (batch_size * seq, features)
        # encoder for start map
        value_1 = self.start_conv_1(value_1)
        value_1 = self.block2d_1_1(value_1)
        value_1 = self.block2d_1_2(value_1)
        value_1 = self.block2d_1_3(value_1)
        value_1 = self.block2d_1_4(value_1)
        value_1 = self.block2d_1_5(value_1)
        value_1 = self.block2d_1_6(value_1)
        value_1 = self.block2d_1_7(value_1)
        value_1 = self.block2d_1_8(value_1)
        value_1 = self.block2d_1_9(value_1)

        # (batch_size * seq, features)
        # encoder for delta maps
        value_2 = self.start_conv_2(value_2)
        value_2 = self.block2d_2_1(value_2)
        value_2 = self.block2d_2_2(value_2)
        value_2 = self.block2d_2_3(value_2)
        value_2 = self.block2d_2_4(value_2)
        value_2 = self.block2d_2_5(value_2)
        value_2 = self.block2d_2_6(value_2)
        value_2 = self.block2d_2_7(value_2)
        value_2 = self.block2d_2_8(value_2)
        value_2 = self.block2d_2_9(value_2)
        value = torch.cat([value_1, value_2], dim=1)
        #value = value_1 + value_2

        value = value.view(-1, self.size_output_layers_encoder_prod)
        value = self.layers_fully_connected(value)

        shape_after_encoder = value.size()
        # seq * batch, hidden_after_fc
        value_seq = torch.zeros((len_max_seq * len_batch, shape_after_encoder[1]))
        if self.args.is_cuda:
            value_seq = value_seq.cuda()
        for idx, idx_seq in enumerate(indexes_values):
            value_seq[idx_seq] = value[idx]

        #(batch_size, seq, features)
        value = value_seq.reshape((len_batch, len_max_seq, shape_after_encoder[1]))

        #(batch, seq, features) => (seq, batch, features)
        value = value.permute(1, 0, 2).contiguous()

        # rnn
        packed = torch.nn.utils.rnn.pack_padded_sequence(value, input_len, batch_first=False) # CUDNN packing
        value_packed, hidden_perm = self.layer_rnn(packed, hidden_perm)
        value, _ = torch.nn.utils.rnn.pad_packed_sequence(value_packed, batch_first=False) # CUDNN packing
        shape_after_rnn = value.size()

        # (seq, batch, features) => (batch, seq, features)
        value = value.permute(1, 0, 2).contiguous()
        value = value.reshape((len_max_seq * len_batch, shape_after_rnn[2]))

        value = value.index_select(dim=0, index=indexes_values)  # reduce data passed through CNN to only sequences without padding

        # decoder
        value = self.layers_fully_connected_2(value)
        value = value.view(
            -1,
            self.size_input_layers_decoder[0],
            self.size_input_layers_decoder[1],
            self.size_input_layers_decoder[2]
        )
        value = self.block_deconv2d_1(value)
        value = self.block_deconv2d_2(value)
        value = self.block_deconv2d_3(value)
        value = self.block_deconv2d_4(value)
        value = self.block_deconv2d_5(value)
        value = self.block_deconv2d_6(value)
        value = self.block_deconv2d_7(value)
        value = self.block_deconv2d_8(value)
        value = self.block_deconv2d_9(value)
        value = self.end_deconv(value)
        shape_after_decoder = value.size()

        # unpack decoder
        value_seq = torch.zeros((len_batch * len_max_seq, shape_after_decoder[1], shape_after_decoder[2], shape_after_decoder[3]))
        if self.args.is_cuda:
            value_seq = value_seq.cuda()
        for idx, idx_seq in enumerate(indexes_values):
            value_seq[idx_seq] = value[idx]

        # batch, seq, channel, w, h
        value = value_seq.reshape((len_batch, len_max_seq, shape_after_decoder[1], shape_after_decoder[2], shape_after_decoder[3]))

        hidden_out = hidden_perm.permute(1, 0, 2).contiguous()

        return value, hidden_out