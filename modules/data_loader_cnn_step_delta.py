import os
import torch
import torch.nn
import torch.utils.data
import json
import logging
import sklearn
import sklearn.model_selection
import numpy as np
import traceback, sys

class Dataset(torch.utils.data.dataset.Dataset):
    def __init__(self, input_maps, output_maps, args):

        super(Dataset, self).__init__()

        self.data = []
        self.args = args

        for i in range(len(input_maps)):
            self.data.append({
                'input': input_maps[i],
                'output': output_maps[i]
            })

        logging.info('dataset size: {}'.format(len(self.data)))

    def __getitem__(self, index):
        result = {
            'x': np.expand_dims(self.data[index]['input'], axis=0).astype(np.float),
            'y': np.expand_dims(self.data[index]['output'], axis=0).astype(np.float)
        }

        vmax = np.amax(result['x'])
        vmin = np.amin(result['x'])
        vdelta = vmax - vmin

        # normalize 0 to 1 for ReLU
        # Softplus higher range 0 to 2
        if self.args.is_data_normalization == 'basic':
            # normalize 0 to 1 for ReLU
            result['y'] = ((result['y'] - vmin) / vdelta)
            result['x'] = ((result['x'] - vmin) / vdelta)
        elif self.args.is_data_normalization == 'zero_centred':
            # normalize -1 to 1
            result['y'] = 2 * ((result['y'] - vmin)/( vmax - vmin)) -1
            result['x'] = 2 * ((result['x'] - vmin)/( vmax - vmin)) -1

        return result


    def __len__(self):
        return len(self.data)


class DataLoader(object):

    @staticmethod
    def get_data_loaders(args):

        maps_list = []
        for filename in os.listdir(args.path_value_maps):
            filename = args.path_value_maps + '/' + filename
            if os.path.isdir(filename):
                maps_list.append(filename)

        input_maps = []
        output_maps = []
        for filename in maps_list:
            try:
                with open(filename + '/metadata.json', 'r') as fp:
                    metadata = json.load(fp)
                map_data = np.memmap(
                    filename + f'/all_maps.memmap',
                    mode='r',
                    dtype=np.float16,
                    shape=tuple(metadata['memmap_shape']))
                # -2 because we neede shape -1 and last 2 maps delta is rounded to 0
                for map_idx in range(map_data.shape[0]-2):
                    input_maps.append(map_data[map_idx])
                    output_maps.append(map_data[map_idx+1]-map_data[map_idx])

            except Exception as e:
                logging.error(filename)
                logging.error(str(e))
                logging.error(f'len(self.data) = {len(input_maps)}')
                exc_type, exc_value, exc_tb = sys.exc_info()
                logging.error(traceback.format_exception(exc_type, exc_value, exc_tb))

        input_list_train, input_list_test, output_list_train, output_list_test = sklearn.model_selection.train_test_split(
            input_maps,
            output_maps,
            test_size=0.2,
            shuffle=False)

        dataset_train = Dataset(input_list_train, output_list_train, args)
        dataset_test = Dataset(input_list_test,output_list_test , args)

        logging.info('train dataset')
        data_loader_train = torch.utils.data.DataLoader(
            dataset_train,
            batch_size=args.batch_size,
            # num_workers=32,
            shuffle=True)

        logging.info('test dataset')
        data_loader_test = torch.utils.data.DataLoader(
            dataset_test,
            batch_size=args.batch_size,
            # num_workers=32,
            shuffle=False)

        return data_loader_train, data_loader_test
