import torch
import torch.nn
import torch.nn.functional as F
import torch.utils.data
import numpy as np
import logging
from torch.nn.parameter import Parameter
import math
from bisect import bisect_right
from functools import partial


def init_parameters(model):
    total_param_size = 0
    for name, param in model.named_parameters():
        each_param_size = np.prod(param.size())
        total_param_size += each_param_size
        logging.info('{} {} {}'.format(name, param.size(), each_param_size))

        if model.args.init_values == 'new':
            if param.requires_grad == True:
                if 'bias' in name:
                    if model.args.init_bias == 'ones':
                        torch.nn.init.constant_(param, 1)
                    if model.args.init_bias == 'zeros': # original
                        torch.nn.init.constant_(param, 0)
                    elif model.args.init_bias == 'uniform':
                        torch.nn.init.uniform_(param)
                else:
                    if 'conv' in name and name.endswith('.weight'):
                        if len(param.size()) > 1:
                            torch.nn.init.kaiming_normal_(param, mode='fan_out', nonlinearity='relu')
                    elif '.bn' in name:
                        if model.args.init_bn == 'ones':
                            torch.nn.init.constant(param, 1)
                        elif model.args.init_bn == 'uniform': # original
                            torch.nn.init.uniform_(param)
                    else:
                        if model.args.init_weights == 'uniform':
                            torch.nn.init.uniform_(param)
                        elif model.args.init_weights == 'xavier_uniform':
                            if len(param.size()) > 1:
                                torch.nn.init.xavier_uniform_(param)
                            else:
                                torch.nn.init.uniform_(param)
                        elif model.args.init_weights == 'xavier_normal': # original
                            if len(param.size()) > 1:
                                torch.nn.init.xavier_normal_(param)
                            else:
                                torch.nn.init.normal_(param)

        elif model.args.init_values == 'old':
            # previous version
            #was used for all non rnn models
            if param.requires_grad == True:
                if len(param.size()) > 1:  # is weight
                    if 'conv' in name and name.endswith('.weight'):
                        torch.nn.init.kaiming_normal_(param, mode='fan_out', nonlinearity='relu')
                    elif '.bn' in name and name.endswith('.weight'):
                        torch.nn.init.constant(param, 1)
                    else:
                        torch.nn.init.xavier_normal_(param)
                else:
                    if 'bias' in name:
                        param.data.zero_()
                    else:
                        torch.nn.init.uniform_(param)
    logging.info(f'total_param_size: {total_param_size}')