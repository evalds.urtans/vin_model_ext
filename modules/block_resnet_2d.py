import torch.nn as nn
import torch.nn.functional as F

# TODO https://github.com/D-X-Y/ResNeXt-DenseNet/blob/master/models/resnext.py
# https://github.com/pytorch/vision/blob/master/torchvision/models/resnet.py


def conv3x3(in_channels, out_channels, stride=1, padding=1):
    """3x3 convolution with padding"""
    return nn.Conv2d(in_channels, out_channels, kernel_size=3, stride=stride,
                     padding=padding, bias=False)


class BasicBlock2D(nn.Module):

    def __init__(self, in_channels, out_channels, stride=1, padding=0):
        super(BasicBlock2D, self).__init__()

        if stride != 1 or in_channels != out_channels:
            self.is_downsample = True
        else:
            self.is_downsample = False

        self.conv1 = conv3x3(in_channels, out_channels, stride, padding+1)
        self.bn1 = nn.BatchNorm2d(out_channels)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = conv3x3(out_channels, out_channels)
        self.bn2 = nn.BatchNorm2d(out_channels)

        if self.is_downsample:
            self.conv_downsample = nn.Conv2d(in_channels, out_channels,  kernel_size=1, stride=stride, padding=padding, bias=False)

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.relu(out)
        out = self.bn1(out)
        out = self.conv2(out)

        if self.is_downsample:
            residual = self.conv_downsample(residual)

        out += residual
        out = self.relu(out)
        out = self.bn2(out)

        return out

class BasicBlock2Dmaxpool(nn.Module):

    def __init__(self, in_channels, out_channels, stride=1):
        super(BasicBlock2Dmaxpool, self).__init__()

        if stride != 1 or in_channels != out_channels:
            self.is_downsample = True
        else:
            self.is_downsample = False

        if stride != 1:
            self.poolLayers = True
            self.poolConv1 = nn.Conv2d(in_channels, out_channels, kernel_size=1, stride=1, bias=False)
            self.maxpool1 = nn.MaxPool2d(kernel_size=3, stride=stride, padding=1, return_indices=True)
        else:
            self.poolLayers = False
            self.conv1 = conv3x3(in_channels, out_channels, stride)

        self.bn1 = nn.BatchNorm2d(out_channels)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = conv3x3(out_channels, out_channels)
        self.bn2 = nn.BatchNorm2d(out_channels)

        if self.is_downsample:
            if stride != 1:
                self.poolConv2 = nn.Conv2d(in_channels, out_channels, kernel_size=1, stride=1, bias=False)
                self.maxpool2 = nn.MaxPool2d(kernel_size=1, stride=stride, return_indices=True)
            else:
                self.conv_downsample = nn.Conv2d(in_channels, out_channels, kernel_size=1, stride=stride, bias=False)

    def forward(self, x):
        ind1 = None
        ind2 = None
        residual = x

        if not self.poolLayers:
            out = self.conv1(x)
        else:
            out = self.poolConv1(x)
            out, ind1 = self.maxpool1(out)

        out = self.relu(out)
        out = self.bn1(out)
        out = self.conv2(out)

        if self.is_downsample:
            if not self.poolLayers:
                residual = self.conv_downsample(residual)
            else:
                residual = self.poolConv2(residual)
                residual, ind2 = self.maxpool2(residual)

        out += residual
        out = self.relu(out)
        out = self.bn2(out)

        return out, ind1, ind2

# Not needed for now

# class Bottleneck2D(nn.Module):
#     expansion = 4
#
#     def __init__(self, in_channels, out_channels, stride=1):
#         super(Bottleneck2D, self).__init__()
#
#         if stride != 1 or in_channels != out_channels * self.expansion:
#             self.is_downsample = True
#         else:
#             self.is_downsample = False
#
#         self.conv1 = nn.Conv2d(in_channels, out_channels, kernel_size=1, bias=False)
#         self.bn1 = nn.BatchNorm2d(out_channels)
#         self.conv2 = nn.Conv2d(out_channels, out_channels, kernel_size=3, stride=stride,
#                                padding=1, bias=False)
#         self.bn2 = nn.BatchNorm2d(out_channels)
#         self.conv3 = nn.Conv2d(out_channels, out_channels * self.expansion, kernel_size=1, bias=False)
#         self.bn3 = nn.BatchNorm2d(out_channels * self.expansion)
#         self.relu = nn.ReLU(inplace=True)
#
#         if stride != 1 or in_channels != out_channels:
#             self.conv_downsample = nn.Conv2d(in_channels, out_channels * self.expansion,  kernel_size=1, stride=stride, bias=False)
#             self.bn_downsample = nn.BatchNorm2d(out_channels * self.expansion)
#
#     def forward(self, x):
#         residual = x
#
#         out = self.conv1(x)
#         out = self.bn1(out)
#         out = self.relu(out)
#
#         out = self.conv2(out)
#         out = self.bn2(out)
#         out = self.relu(out)
#
#         out = self.conv3(out)
#         out = self.bn3(out)
#
#         if self.is_downsample:
#             x = self.conv_downsample(x)
#             residual = self.bn_downsample(x)
#
#         out += residual
#         out = self.relu(out)
#
#         return out