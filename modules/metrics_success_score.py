import numpy as np
import torch
import multiprocessing
import ctypes
import time
import traceback, sys, logging


class ProcessMetricSuccessRate (multiprocessing.Process):
    def __init__(self, sample_size, map_size):
        super(ProcessMetricSuccessRate, self).__init__()

        # coppied variables (only basic datatypes)
        self.sample_size = sample_size
        self.map_size = map_size

        # shared variables 1D
        self.target_map = multiprocessing.Array(ctypes.c_float, sample_size * map_size * map_size)
        self.input_map = multiprocessing.Array(ctypes.c_float, sample_size * map_size * map_size)
        self.success_map = multiprocessing.Array(ctypes.c_float, sample_size * map_size * map_size)
        self.scores = multiprocessing.Array(ctypes.c_float, sample_size)
        self.success_rates = multiprocessing.Array(ctypes.c_float, sample_size)

    def run(self):
        try:

            target_map = np.array(self.target_map).reshape(self.sample_size, self.map_size, self.map_size)
            input_map = np.array(self.input_map).reshape(self.sample_size, self.map_size, self.map_size)
            success_map = np.array(self.success_map).reshape(self.sample_size, self.map_size, self.map_size)
            max_value = np.max(target_map)
            min_value = np.min(target_map)
            goal_map = (target_map == max_value)
            wall_map = (target_map == min_value)
            # get wall count for later use
            wall_count = np.zeros(np.size(input_map, 0))
            for n in range(np.size(input_map, 0)):
                walls = np.where(wall_map[n] == True)
                wall_count[n] = np.size(walls, 1)

            score_map = np.zeros(input_map.shape, dtype=int)
            policy_map = np.zeros(input_map.shape, dtype=int)
            state_count = np.size(input_map, 1) * np.size(input_map, 2) - wall_count
            # 4 directional movement [u, r, d, l] as [1, 2, 3, 4]

            # iterate through input map
            for n in range(np.size(input_map, 0)):
                for y in range(np.size(input_map, 1)):
                    for x in range(np.size(input_map, 2)):
                        # if not wall or goal
                        if not wall_map[n, y, x]:
                            if not goal_map[n, y, x]:
                                # get neighbor state values and choose smallest
                                actions = []

                                if y > 0:
                                    actions.append(input_map[n, y - 1, x])
                                else:
                                    actions.append(float('-Inf'))

                                if x < input_map.shape[2] - 1:
                                    actions.append(input_map[n, y, x + 1])
                                else:
                                    actions.append(float('-Inf'))

                                if y < input_map.shape[1] - 1:
                                    actions.append(input_map[n, y + 1, x])
                                else:
                                    actions.append(float('-Inf'))

                                if x > 0:
                                    actions.append(input_map[n, y, x - 1])
                                else:
                                    actions.append(float('-Inf'))

                                bestAction = np.argmax(np.array(actions))
                                # put biggest action in policy map
                                # if bestAction > input_map[n, y, x]:
                                if bestAction == 0:
                                    policy_map[n, y, x] = 1
                                elif bestAction == 1:
                                    policy_map[n, y, x] = 2
                                elif bestAction == 2:
                                    policy_map[n, y, x] = 3
                                elif bestAction == 3:
                                    policy_map[n, y, x] = 4

            startPoints = np.zeros((np.size(input_map, 0), 2, 1), dtype=int)
            for n in range(np.size(input_map, 0)):
                # find start point x and y for all maps
                startFlat = np.argmax(goal_map[n])
                startY = startFlat // np.size(input_map, 1)
                startX = startFlat - startY * np.size(input_map, 1)
                startPoints[n, 0] = int(startY)
                startPoints[n, 1] = int(startX)

            # go back from start point to all accessible states
            for n in range(np.size(input_map, 0)):
                openList = []
                openList.append(startPoints[n])
                while openList:
                    # access first element of array
                    if openList[0][0] > 0:  # y
                        if policy_map[n, openList[0][0] - 1, openList[0][1]] == 3:
                            openList.append(np.array([openList[0][0] - 1, openList[0][1]]))
                            score_map[n, openList[0][0] - 1, openList[0][1]] = score_map[n, openList[0][0], openList[0][
                                1]] + 1

                    if openList[0][1] < input_map.shape[2] - 1:  # x
                        if policy_map[n, openList[0][0], openList[0][1] + 1] == 4:
                            openList.append(np.array([openList[0][0], openList[0][1] + 1]))
                            score_map[n, openList[0][0], openList[0][1] + 1] = score_map[n, openList[0][0], openList[0][
                                1]] + 1

                    if openList[0][0] < input_map.shape[1] - 1:  # y
                        if policy_map[n, openList[0][0] + 1, openList[0][1]] == 1:
                            openList.append(np.array([openList[0][0] + 1, openList[0][1]]))
                            score_map[n, openList[0][0] + 1, openList[0][1]] = score_map[n, openList[0][0], openList[0][
                                1]] + 1

                    if openList[0][1] > 0:  # x
                        if policy_map[n, openList[0][0], openList[0][1] - 1] == 2:
                            openList.append(np.array([openList[0][0], openList[0][1] - 1]))
                            score_map[n, openList[0][0], openList[0][1] - 1] = score_map[n, openList[0][0], openList[0][
                                1]] + 1
                    del openList[0]

            # get passage count which where visited
            visitedStateCount = np.zeros(np.size(input_map, 0))
            score = np.zeros(np.size(input_map, 0))
            success_rate = np.zeros(np.size(input_map, 0))
            # calculate score and success rate
            for n in range(np.size(input_map, 0)):
                visited = np.where(score_map[n] > 0)
                visitedStateCount[n] = np.size(visited, 1) + 1
                score[n] = np.sum(score_map[n])
                unvisitedStateCount = state_count[n] - visitedStateCount[n]
                score[n] = score[n] + unvisitedStateCount * state_count[n]
                success_rate[n] = visitedStateCount[n] / state_count[n]

                for y in range(np.size(input_map, 1)):
                    for x in range(np.size(input_map, 2)):
                        if score_map[n, y, x] > 0 and not goal_map[n, y, x]:
                            success_map[n, y, x] = 1
                        elif goal_map[n, y, x]:
                            success_map[n, y, x] = 2
                        elif not wall_map[n, y, x]:
                            success_map[n, y, x] = 0.5

            self.scores[:] = score
            self.success_rates[:] = success_rate
            self.success_map[:] = success_map.reshape((self.sample_size * self.map_size * self.map_size,))

        except Exception as e:
            logging.error(str(e))
            exc_type, exc_value, exc_tb = sys.exc_info()
            logging.error(traceback.format_exception(exc_type, exc_value, exc_tb))


class Metrics_success_score:

    @staticmethod
    def metric_success_rate(input, target, thread_count=8):

        target_map = target.view(-1, input.size(2), input.size(3)).numpy()
        input_map = input.view(-1, input.size(2), input.size(3)).detach().numpy()
        success_map = np.copy(target_map)

        sample_size = max(1, int(target_map.shape[0] / thread_count))
        sample_sizes = []
        tmp_counter = 0
        for idx_thread in range(thread_count):
            sample_sizes.append(sample_size)
            tmp_counter += sample_size
            if tmp_counter + sample_size > target_map.shape[0]:
                break
        if tmp_counter < target_map.shape[0]:
            sample_sizes[-1] += (target_map.shape[0] - tmp_counter)

        processes = []
        idx_in_batch_sample = 0
        for sample_size in sample_sizes:
            process = ProcessMetricSuccessRate(sample_size, map_size=input.size(2))
            process.input_map[:] = input_map[idx_in_batch_sample:idx_in_batch_sample + sample_size].flatten().tolist()
            process.target_map[:] = target_map[idx_in_batch_sample:idx_in_batch_sample + sample_size].flatten().tolist()
            process.success_map[:] = success_map[idx_in_batch_sample:idx_in_batch_sample + sample_size].flatten().tolist()
            processes.append(process)
            idx_in_batch_sample += sample_size

        if thread_count == 1:
            process.run() # easier for debugging
        else:
            for process in processes:
                process.start()

            for process in processes:
                process.join()

        scores = np.zeros((target_map.shape[0], ))
        success_rates = np.zeros((target_map.shape[0],))

        # aggregate data
        idx_in_batch_sample = 0
        for process in processes:
            scores[idx_in_batch_sample:idx_in_batch_sample+process.sample_size] = process.scores[:]
            success_rates[idx_in_batch_sample:idx_in_batch_sample + process.sample_size] = process.success_rates[:]

            each_success_map = np.array(process.success_map).reshape(process.sample_size, input.size(2), input.size(2))
            success_map[idx_in_batch_sample:idx_in_batch_sample + process.sample_size] = each_success_map
            idx_in_batch_sample += process.sample_size

        score = np.mean(scores)
        success_rate = np.mean(success_rates)

        return score, success_rate, success_map
