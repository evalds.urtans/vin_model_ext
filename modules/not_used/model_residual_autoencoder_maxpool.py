import torch
import torch.nn
import torch.utils.data
import numpy as np
from modules.block_resnet_2d import BasicBlock2Dmaxpool
from modules.block_resnet_deconv2d import DeconvBlock2Dmaxpool
import modules.torch_utils as torch_utils

class Model(torch.nn.Module):
    def __init__(self, args):
        super(Model, self).__init__()
        self.args = args

        # input 32 x 32

        self.start_conv = torch.nn.Conv2d(in_channels=1, out_channels=32, kernel_size=8, bias=False)
        # 25x25x32
        self.block2d_1 = BasicBlock2Dmaxpool(in_channels=32, out_channels=32, stride=1)
        # 25x25x32
        self.block2d_2 = BasicBlock2Dmaxpool(in_channels=32, out_channels=64, stride=2)
        # 13x13x64
        self.block2d_3 = BasicBlock2Dmaxpool(in_channels=64, out_channels=64, stride=1)
        # 13x13x64
        self.block2d_4 = BasicBlock2Dmaxpool(in_channels=64, out_channels=128, stride=2)
        # 7x7x128

        self.size_output_layers_encoder = [128, 7, 7]
        self.size_output_layers_encoder_prod = int(np.prod(self.size_output_layers_encoder))

        # add as parameter
        self.compression_fc = 0.25
        self.fc_hidden_size = int(self.size_output_layers_encoder_prod * self.compression_fc)

        self.layers_fc = torch.nn.Sequential(
            torch.nn.Linear(in_features=self.size_output_layers_encoder_prod, out_features=self.fc_hidden_size),
            torch.nn.ReLU(),
            torch.nn.Linear(in_features=self.fc_hidden_size, out_features=self.size_output_layers_encoder_prod),
            torch.nn.ReLU()
        )

        self.block_deconv2d_1 = DeconvBlock2Dmaxpool(in_channels=128, out_channels=64, stride=2)
        # 13x13x64
        self.block_deconv2d_2 = DeconvBlock2Dmaxpool(in_channels=64, out_channels=64, stride=1)
        # 13x13x64
        self.block_deconv2d_3 = DeconvBlock2Dmaxpool(in_channels=64, out_channels=32, stride=2)
        # 25x25x32
        self.block_deconv2d_4 = DeconvBlock2Dmaxpool(in_channels=32, out_channels=32, stride=1)
        # 25x25x32
        self.end_deconv = torch.nn.ConvTranspose2d(in_channels=32, out_channels=1, kernel_size=8, bias=False)
        # 32x32x1

        if not self.args.preload_weights_filename == "none":
            self.load_state_dict(torch.load(self.args.preload_weights_filename))
            for name, param in self.named_parameters():
                if 'block2d' in name:
                    param.requires_grad = False
                elif 'start_conv' in name:
                    param.requires_grad = False
        torch_utils.init_parameters(self)


    def forward(self, value):

        residual_1 = self.start_conv(value)
        residual_2, _, _ = self.block2d_1(residual_1)
        residual_3, ind1_1, ind1_2 = self.block2d_2(residual_2)
        residual_4, _, _ = self.block2d_3(residual_3)
        out, ind2_1, ind2_2  = self.block2d_4(residual_4)

        # fully connected
        out = out.view(-1, self.size_output_layers_encoder_prod)
        out = self.layers_fc(out)
        out = out.view(
            -1,
            self.size_output_layers_encoder[0],
            self.size_output_layers_encoder[1],
            self.size_output_layers_encoder[2]
        )

        # UNet architecture(residual connections from encoder to decoder)
        if self.args.is_residual == True:
            out = self.block_deconv2d_1(out, ind2_1, ind2_2) + residual_4
            out = self.block_deconv2d_2(out) + residual_3
            out = self.block_deconv2d_3(out, ind1_1, ind1_2) + residual_2
            out = self.block_deconv2d_4(out) + residual_1
            out = self.end_deconv(out)
        else:
            out = self.block_deconv2d_1(out, ind2_1, ind2_2)
            out = self.block_deconv2d_2(out)
            out = self.block_deconv2d_3(out, ind1_1, ind1_2)
            out = self.block_deconv2d_4(out)
            out = self.end_deconv(out)

        return out