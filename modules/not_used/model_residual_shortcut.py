import torch
import torch.nn
import torch.nn.functional as F
import torch.utils.data
import numpy as np
import logging
import modules.torch_utils as torch_utils

class Model(torch.nn.Module):
    def __init__(self, args):
        super(Model, self).__init__()
        self.args = args

        # ref http://arxiv.org/abs/1412.6806
        # https://nrupatunga.github.io/convolution-2/

        # formula for size of conv O = \frac{(W-K+2P)}{S} + 1
        # K - kernel size
        # W - input size
        # P - padding
        # S - stride

        # formula deconv O = S(W-1)+K-2P

        # input 32 x 32

        self.layers_encoder_1 = torch.nn.Sequential(
            torch.nn.Conv2d(in_channels=1, out_channels=8, kernel_size=4, stride=2, padding=1),
            # 16x16x8
            torch.nn.ReLU()
        )

        self.layers_encoder_2 = torch.nn.Sequential(
            torch.nn.Conv2d(in_channels=8, out_channels=16, kernel_size=4, stride=2, padding=1),
            # 8x8x16
            torch.nn.BatchNorm2d(num_features=16)
        )

        self.layers_conv_deconv = torch.nn.Sequential(
            torch.nn.ReLU(),
            torch.nn.Conv2d(in_channels=16, out_channels=32, kernel_size=4, stride=2, padding=1),
            # 4x4x32
            torch.nn.BatchNorm2d(num_features=32),
            torch.nn.ReLU(),
            torch.nn.Conv2d(in_channels=32, out_channels=32, kernel_size=3, stride=1, padding=1),
            # 4x4x64
            torch.nn.BatchNorm2d(num_features=32),
            torch.nn.ConvTranspose2d(in_channels=32, out_channels=32, kernel_size=3, stride=2, padding=1),
            # 7x7x32
            torch.nn.BatchNorm2d(num_features=32),
            torch.nn.ReLU(),
            torch.nn.ConvTranspose2d(in_channels=32, out_channels=16, kernel_size=4, stride=1, padding=1),
            # 8x8x16
            torch.nn.BatchNorm2d(num_features=16)
        )

        self.layers_decoder_1 = torch.nn.Sequential(
            torch.nn.ConvTranspose2d(in_channels=16, out_channels=8, kernel_size=4, stride=2, padding=1),
            # 16x16x8
            torch.nn.BatchNorm2d(num_features=8)
        )

        self.layers_decoder_2 = torch.nn.Sequential(
            torch.nn.ConvTranspose2d(in_channels=8, out_channels=1, kernel_size=4, stride=2, padding=1),
            # 32x32x1
            torch.nn.BatchNorm2d(num_features=1),
            torch.nn.ReLU()
        )

        torch_utils.init_parameters(self)

    def forward(self, value):

        value = self.layers_encoder_1(value)
        res1 = value
        value = self.layers_encoder_2(value)
        res2 = value
        value = self.layers_conv_deconv(value)
        value = value + res2
        value = self.layers_decoder_1(value)
        value = value + res1
        value = self.layers_decoder_2(value)

        return value