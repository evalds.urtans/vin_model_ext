import torch
import torch.nn
import torch.nn.functional as F
import torch.utils.data
import numpy as np
import logging
import modules.torch_utils as torch_utils

class Model(torch.nn.Module):
    def __init__(self, args):
        super(Model, self).__init__()
        self.args = args

        # ref http://arxiv.org/abs/1412.6806
        # https://nrupatunga.github.io/convolution-2/

        # formula for size of conv O = \frac{(W-K+2P)}{S} + 1
        # K - kernel size
        # W - input size
        # P - padding
        # S - stride

        # formula deconv O = S(W-1)+K-2P

        
        # input 32 x 32

        self.layers_encoder = torch.nn.Sequential(
            torch.nn.Conv2d(in_channels=1, out_channels=8, kernel_size=4, stride=1, padding=0),
            # (32-4+2*0)/1 = 28*28*8 =﻿6272
            torch.nn.ReLU(),
            torch.nn.Conv2d(in_channels=8, out_channels=16, kernel_size=4, stride=2),
            # (28-4+2*0)/2 = 12*12*16 =﻿﻿2304
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=16),
            torch.nn.Dropout(p=self.args.dropout),
            torch.nn.Conv2d(in_channels=16, out_channels=32, kernel_size=4, padding=2),
            # (12-4+2*2)/1 = 12*12*32 =﻿4608
            torch.nn.ReLU(),
            torch.nn.Conv2d(in_channels=32, out_channels=32, kernel_size=4, padding=2, dilation=2),
            # (12-4+2*2)/1 = 12*12*32 =﻿4608
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=32),
            torch.nn.Dropout(p=self.args.dropout)
        )

        self.size_output_layers_encoder = [32, 12, 12]
        self.size_output_layers_encoder_prod = int(np.prod(self.size_output_layers_encoder))

        self.compression_fc = 0.25
        self.fc_hidden_size = int(self.size_output_layers_encoder_prod * self.compression_fc)

        self.layers_fc = torch.nn.Sequential(
            torch.nn.Linear(in_features=self.size_output_layers_encoder_prod, out_features=self.fc_hidden_size),
            torch.nn.ReLU(),
            torch.nn.Linear(in_features=self.fc_hidden_size, out_features=self.size_output_layers_encoder_prod),
            torch.nn.ReLU()
        )

        self.layers_decoder = torch.nn.Sequential(
            torch.nn.ConvTranspose2d(in_channels=32, out_channels=16, kernel_size=4),
            # O = 1*(12-1)+4-2*0 = 15
            torch.nn.ReLU(),
            torch.nn.ConvTranspose2d(in_channels=16, out_channels=8, kernel_size=4),
            # O = 1*(15-1)+4-2*0 = 18
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=8),
            torch.nn.Dropout(p=self.args.dropout),
            torch.nn.ConvTranspose2d(in_channels=8, out_channels=4, kernel_size=9),
            # O = 1*(18-1)+9-2*0 = 26
            torch.nn.ReLU(),
            torch.nn.ConvTranspose2d(in_channels=4, out_channels=1, kernel_size=7),
            # O = 1*(26-1)+7-2*0 = 32
            torch.nn.ReLU()
        )

        torch_utils.init_parameters(self)

    def forward(self, value):

        value = self.layers_encoder(value)
        value = value.view(-1, self.size_output_layers_encoder_prod)
        value = self.layers_fc(value)
        value = value.view(
            -1,
            self.size_output_layers_encoder[0],
            self.size_output_layers_encoder[1],
            self.size_output_layers_encoder[2]
        )
        value = self.layers_decoder(value)

        return value