import os
import torch
import torch.nn
import torch.utils.data
import json
import logging
import sklearn
import sklearn.model_selection
import numpy as np
import traceback, sys

class Dataset(torch.utils.data.dataset.Dataset):
    def __init__(self, maps_list):

        super(Dataset, self).__init__()

        self.data = []

        for filename in maps_list:
            try:
                with open(filename + '/metadata.json', 'r') as fp:
                    metadata = json.load(fp)

                input = np.memmap(
                    filename + f'/0.memmap',
                    mode='r',
                    dtype=np.float16,
                    shape=tuple(metadata['memmap_shape']))

                output = np.memmap(
                    filename + f'/0.memmap',
                    mode='r',
                    dtype=np.float16,
                    shape=tuple(metadata['memmap_shape']))

                self.data.append({
                    'x': np.expand_dims(input, axis=0),
                    'y': np.expand_dims(output, axis=0)
                })

            except Exception as e:
                logging.error(filename)
                logging.error(str(e))
                exc_type, exc_value, exc_tb = sys.exc_info()
                logging.error(traceback.format_exception(exc_type, exc_value, exc_tb))

        logging.info('dataset size: {}'.format(len(self.data)))

    def __getitem__(self, index):
        result = self.data[index]
        result['y'] = np.array(result['y']).astype(np.float)
        result['x'] = np.array(result['x']).astype(np.float)

        vmax = np.amax(result['y'])
        vmin = np.amin(result['y'])
        vdelta = vmax - vmin

        # normalize 0 to 1 for ReLU
        # Softplus higher range 0 to 2
        result['y'] = ((result['y'] - vmin) / vdelta)
        result['x'] = ((result['x'] - vmin) / vdelta)
        return result


    def __len__(self):
        return len(self.data)


class DataLoader(object):

    @staticmethod
    def get_data_loaders(args):

        maps_list = []
        for filename in os.listdir(args.path_value_maps):
            filename = args.path_value_maps + '/' + filename
            if os.path.isdir(filename):
                maps_list.append(filename)

        maps_list_train, maps_list_test = sklearn.model_selection.train_test_split(
            maps_list,
            test_size=0.2,
            shuffle=False)

        dataset_train = Dataset(maps_list_train)
        dataset_test = Dataset(maps_list_test)

        logging.info('train dataset')
        data_loader_train = torch.utils.data.DataLoader(
            dataset_train,
            batch_size=args.batch_size,
            #num_workers=16,
            shuffle=True)

        logging.info('test dataset')
        data_loader_test = torch.utils.data.DataLoader(
            dataset_test,
            batch_size=args.batch_size,
            #num_workers=16,
            shuffle=False)

        return data_loader_train, data_loader_test
