import torch
import torch.nn
import torch.nn.functional as F
import torch.utils.data
import numpy as np
import logging
import modules.torch_utils as torch_utils


class Model(torch.nn.Module):
    def __init__(self, args):
        super(Model, self).__init__()
        self.args = args

        # input 32 x 32

        self.first = torch.nn.Sequential(
            torch.nn.Conv2d(in_channels=1, out_channels=8, kernel_size=4),
            # 29x29x8
            torch.nn.ReLU(),
            torch.nn.Conv2d(in_channels=8, out_channels=16, kernel_size=4),
            # 26x26x16
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=16)
        )

        self.first_residual_identity = torch.nn.Sequential(
            torch.nn.Conv2d(in_channels=16, out_channels=8, kernel_size=1),
            # 26x26x16
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=8),
            torch.nn.Conv2d(in_channels=8, out_channels=8, kernel_size=3, padding=1),
            # 26x26x16
            torch.nn.ReLU(),
            torch.nn.Conv2d(in_channels=8, out_channels=16, kernel_size=1),
            # 26x26x16
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=16)
        )

        self.between_residual_identity = torch.nn.Sequential(
            torch.nn.ReLU(),
            torch.nn.Conv2d(in_channels=16, out_channels=32, kernel_size=4, stride=2),
            # 12x12x32
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=32)
        )

        self.second_residual_identity = torch.nn.Sequential(
            torch.nn.Conv2d(in_channels=32, out_channels=16, kernel_size=1),
            # 12x12x16
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=16),
            torch.nn.Conv2d(in_channels=16, out_channels=16, kernel_size=3, padding=1),
            # 12x12x16
            torch.nn.ReLU(),
            torch.nn.Conv2d(in_channels=16, out_channels=32, kernel_size=1),
            # 12x12x32
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=32)
        )

        self.after_residual_identity = torch.nn.Sequential(
            torch.nn.ReLU(),
            torch.nn.Conv2d(in_channels=32, out_channels=64, kernel_size=4, stride=2, padding=1),
            # 6x6x64
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=64)
        )

        self.first_residual_bottleneck = torch.nn.Sequential(
            torch.nn.Conv2d(in_channels=16, out_channels=32, kernel_size=3, padding=1),
            # 26x26x32
            torch.nn.ReLU(),
            torch.nn.Conv2d(in_channels=32, out_channels=32, kernel_size=3, padding=1),
            # 26x26x32
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=32)
        )

        self.first_shortcut = torch.nn.Sequential(
            torch.nn.Conv2d(in_channels=16, out_channels=32, kernel_size=1),
            # 26x26x32
            torch.nn.BatchNorm2d(num_features=32)
        )

        self.second_residual_bottleneck = torch.nn.Sequential(
            torch.nn.ReLU(),
            torch.nn.Conv2d(in_channels=32, out_channels=64, kernel_size=4, dilation=2),
            # 20x20x64
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=64),
            torch.nn.Conv2d(in_channels=64, out_channels=64, kernel_size=4, padding=1, dilation=2),
            # 16x16x64
            torch.nn.ReLU(),
            torch.nn.Conv2d(in_channels=64, out_channels=64, kernel_size=4),
            # 13x13x64
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=64),
        )

        self.second_shortcut = torch.nn.Sequential(
            torch.nn.Conv2d(in_channels=32, out_channels=64, kernel_size=1, stride=2),
            # 13x13x64
            torch.nn.BatchNorm2d(num_features=64),
        )

        self.after_bottleneck = torch.nn.Sequential(
            torch.nn.ReLU(),
            torch.nn.Conv2d(in_channels=64, out_channels=64, kernel_size=4, stride=2, padding=1),
            # 6x6x64
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=64)
        )


        self.size_output_layers_encoder = [64, 6, 6]
        self.size_output_layers_encoder_prod = int(np.prod(self.size_output_layers_encoder))

        self.compression_fc = 0.25
        self.fc_hidden_size = int(self.size_output_layers_encoder_prod * self.compression_fc)

        self.layers_fc = torch.nn.Sequential(
            torch.nn.Linear(in_features=self.size_output_layers_encoder_prod, out_features=self.fc_hidden_size),
            torch.nn.ReLU(),
            torch.nn.Linear(in_features=self.fc_hidden_size, out_features=self.size_output_layers_encoder_prod),
            torch.nn.ReLU()
        )

        self.layers_decoder = torch.nn.Sequential(
            torch.nn.ConvTranspose2d(in_channels=64, out_channels=32, kernel_size=4),
            # 9x9x32
            torch.nn.ReLU(),
            torch.nn.ConvTranspose2d(in_channels=32, out_channels=16, kernel_size=6),
            # 14x14x16
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=16),
            torch.nn.ConvTranspose2d(in_channels=16, out_channels=8, kernel_size=8),
            # 21x21x8
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=8),
            torch.nn.ConvTranspose2d(in_channels=8, out_channels=4, kernel_size=8),
            # 28x28x4
            torch.nn.ConvTranspose2d(in_channels=4, out_channels=1, kernel_size=5),
            # 32x32x1
            torch.nn.ReLU()
        )

        if not self.args.preload_weights_filename == "none":
            self.load_state_dict(torch.load(self.args.preload_weights_filename))

        torch_utils.init_parameters(self)

    def forward(self, value):

        if self.args.is_residual == True:
            value = self.first(value)
            residual = value
            value = self.first_residual_identity(value)
            value = residual + value
            value = self.between_residual_identity(value)
            residual = value
            value = self.second_residual_identity(value)
            value = residual + value
            value = self.after_residual_identity(value)
        else:
            value = self.first(value)
            residual = value
            value = self.first_residual_bottleneck(value)
            residual = self.first_shortcut(residual)
            value = residual + value
            residual = value
            value = self.second_residual_bottleneck(value)
            residual = self.second_shortcut(residual)
            value = residual + value
            value = self.after_bottleneck(value)

        value = value.view(-1, self.size_output_layers_encoder_prod)
        value = self.layers_fc(value)
        value = value.view(
            -1,
            self.size_output_layers_encoder[0],
            self.size_output_layers_encoder[1],
            self.size_output_layers_encoder[2]
        )

        value = self.layers_decoder(value)

        return value