import torch
import torch.nn
import torch.nn.functional as F
import torch.utils.data
import numpy as np
import logging
import modules.torch_utils as torch_utils

class Model(torch.nn.Module):
    def __init__(self, args):
        super(Model, self).__init__()
        self.args = args

        # input 32 x 32

        self.conv_first = torch.nn.Conv2d(in_channels=1, out_channels=32, kernel_size=7)
        # 26x26x32
        # layers 1

        self.conv_resBlock_first = torch.nn.Sequential(
            torch.nn.ReLU(),
            torch.nn.Conv2d(in_channels=32, out_channels=32, kernel_size=3, padding=1),
            # 26x26x32
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=32),
            torch.nn.Conv2d(in_channels=32, out_channels=32, kernel_size=3, padding=1)
            # 26x26x32
        )
        # layers 3

        self.conv_resBlock_second = torch.nn.Sequential(
            # ReLu after residual addition
            torch.nn.ReLU(),

            torch.nn.Conv2d(in_channels=32, out_channels=32, kernel_size=3, padding=1),
            # 26x26x32
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=32),
            torch.nn.Conv2d(in_channels=32, out_channels=32, kernel_size=3, padding=1)
            # 26x26x32
        )
        # layers 5

        self.conv_bottleneck_to_third = torch.nn.Sequential(
            torch.nn.ReLU(),
            torch.nn.Conv2d(in_channels=32, out_channels=64, kernel_size=1, stride=2)
            # 13x13x64
        )

        self.conv_resBlock_third = torch.nn.Sequential(
            # ReLu after residual addition
            torch.nn.ReLU(),

            torch.nn.Conv2d(in_channels=32, out_channels=64, kernel_size=3, padding=1, stride=2),
            # 13x13x64
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=64),
            torch.nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, padding=1)
            # 13x13x64
        )
        # layers 7

        self.conv_resBlock_fourth = torch.nn.Sequential(
            # ReLu after residual addition
            torch.nn.ReLU(),

            torch.nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, padding=1),
            # 13x13x64
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=64),
            torch.nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, padding=1)
            # 13x13x64
        )
        # layers 9

        self.conv_bottleneck_to_fifth = torch.nn.Sequential(
            torch.nn.ReLU(),
            torch.nn.Conv2d(in_channels=64, out_channels=128, kernel_size=1, stride=2)
            # 7x7x128
        )

        self.conv_resBlock_fifth = torch.nn.Sequential(
            # ReLu after residual addition
            torch.nn.ReLU(),

            torch.nn.Conv2d(in_channels=64, out_channels=128, kernel_size=3, padding=1, stride=2),
            # 7x7x128
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=128),
            torch.nn.Conv2d(in_channels=128, out_channels=128, kernel_size=3, padding=1)
            # 7x7x128
        )
        # layers 13

        self.conv_resBlock_sixth = torch.nn.Sequential(
            # ReLu after residual addition
            torch.nn.ReLU(),

            torch.nn.Conv2d(in_channels=128, out_channels=128, kernel_size=3, padding=1),
            # 7x7x128
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=128),
            torch.nn.Conv2d(in_channels=128, out_channels=128, kernel_size=3, padding=1)
            # 7x7x128
        )
        # layers 15

        # maybe add Avg or Max pooling layer
        # self.pooling = torch.nn.MaxPool2d()


        self.size_output_layers_encoder = [128, 7, 7]
        self.size_output_layers_encoder_prod = int(np.prod(self.size_output_layers_encoder))

        # add as parameter
        self.compression_fc = 0.25
        self.fc_hidden_size = int(self.size_output_layers_encoder_prod * self.compression_fc)

        self.layers_fc = torch.nn.Sequential(
            torch.nn.Linear(in_features=self.size_output_layers_encoder_prod, out_features=self.fc_hidden_size),
            torch.nn.ReLU(),
            torch.nn.Linear(in_features=self.fc_hidden_size, out_features=self.size_output_layers_encoder_prod),
            torch.nn.ReLU()
        )

        self.layers_decoder = torch.nn.Sequential(
            torch.nn.ConvTranspose2d(in_channels=128, out_channels=64, kernel_size=5),
            # 11x11x64
            torch.nn.ReLU(),
            torch.nn.ConvTranspose2d(in_channels=64, out_channels=32, kernel_size=5),
            # 15x15x32
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=32),
            torch.nn.ConvTranspose2d(in_channels=32, out_channels=16, kernel_size=8),
            # 22x22x16
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=16),
            torch.nn.ConvTranspose2d(in_channels=16, out_channels=8, kernel_size=6),
            # 27x27x8
            torch.nn.ConvTranspose2d(in_channels=8, out_channels=1, kernel_size=6),
            # 32x32x1
            torch.nn.ReLU()
        )

        if not self.args.preload_weights_filename == "none":
            self.load_state_dict(torch.load(self.args.preload_weights_filename))

        for name, param in self.named_parameters():
            if 'conv' in name:
                param.requires_grad = False


        torch_utils.init_parameters(self)



    def forward(self, value):

        # encoder
        if self.args.is_residual == True:
            value = self.conv_first(value)
            value = value + self.conv_resBlock_first(value)
            value = value + self.conv_resBlock_second(value)
            value = self.conv_bottleneck_to_third(value) + self.conv_resBlock_third(value)
            value = value + self.conv_resBlock_fourth(value)
            value = self.conv_bottleneck_to_fifth(value) + self.conv_resBlock_fifth(value)
            value = value + self.conv_resBlock_sixth(value)
        else:
            value = self.conv_first(value)
            value = self.conv_resBlock_first(value)
            value = self.conv_resBlock_second(value)
            value = self.conv_resBlock_third(value)
            value = self.conv_resBlock_fourth(value)
            value = self.conv_resBlock_fifth(value)
            value = self.conv_resBlock_sixth(value)

        # fully connected
        value = value.view(-1, self.size_output_layers_encoder_prod)
        value = self.layers_fc(value)
        value = value.view(
            -1,
            self.size_output_layers_encoder[0],
            self.size_output_layers_encoder[1],
            self.size_output_layers_encoder[2]
        )

        # decoder
        value = self.layers_decoder(value)

        return value