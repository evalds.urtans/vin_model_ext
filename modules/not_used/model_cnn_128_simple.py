import torch
import torch.nn
import torch.nn.functional as F
import torch.utils.data
import numpy as np
import logging
import modules.torch_utils as torch_utils


class Model(torch.nn.Module):
    def __init__(self, args):
        super(Model, self).__init__()
        self.args = args

        # input 128 x 128

        self.layers_encoder = torch.nn.Sequential(
            torch.nn.Conv2d(in_channels=1, out_channels=8, kernel_size=4, stride=1, padding=0),
            # 125x125
            torch.nn.ReLU(),
            torch.nn.Conv2d(in_channels=8, out_channels=16, kernel_size=4, stride=2, padding=2),
            # 63x63
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=16),
            torch.nn.Dropout(p=self.args.dropout),
            torch.nn.Conv2d(in_channels=16, out_channels=16, kernel_size=4),
            # torch.nn.MaxPool2d(kernel_size=4, padding=0, stride=1, dilation=1),
            # 60x60
            torch.nn.ReLU(),
            torch.nn.Conv2d(in_channels=16, out_channels=32, kernel_size=4, stride=2),
            # 29x29
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=32),
            torch.nn.Dropout(p=self.args.dropout),
            torch.nn.Conv2d(in_channels=32, out_channels=64, kernel_size=4),
            # 26x26
            torch.nn.ReLU(),
            torch.nn.Conv2d(in_channels=64, out_channels=64, kernel_size=4, stride=2),
            # 12x12
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=64),
            torch.nn.Dropout(p=self.args.dropout)
        )

        self.size_output_layers_encoder = [64, 12, 12]
        self.size_output_layers_encoder_prod = int(np.prod(self.size_output_layers_encoder))

        self.compression_fc = 0.25
        self.fc_hidden_size = int(self.size_output_layers_encoder_prod * self.compression_fc)

        self.layers_fc = torch.nn.Sequential(
            torch.nn.Linear(in_features=self.size_output_layers_encoder_prod, out_features=self.fc_hidden_size),
            torch.nn.ReLU(),
            torch.nn.Linear(in_features=self.fc_hidden_size, out_features=self.size_output_layers_encoder_prod),
            torch.nn.ReLU()
        )

        self.layers_decoder = torch.nn.Sequential(
            torch.nn.ConvTranspose2d(in_channels=64, out_channels=64, kernel_size=9),
            # 20x20
            torch.nn.ReLU(),
            torch.nn.ConvTranspose2d(in_channels=64, out_channels=32, kernel_size=5, stride=2),
            # 43x43
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=32),
            torch.nn.Dropout(p=self.args.dropout),
            torch.nn.ConvTranspose2d(in_channels=32, out_channels=32, kernel_size=8),
            # 50x50
            torch.nn.ReLU(),
            torch.nn.ConvTranspose2d(in_channels=32, out_channels=16, kernel_size=9, stride=2),
            # 107x107
            torch.nn.ReLU(),
            torch.nn.ConvTranspose2d(in_channels=16, out_channels=16, kernel_size=8),
            # 114x114
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=16),
            torch.nn.Dropout(p=self.args.dropout),
            torch.nn.ConvTranspose2d(in_channels=16, out_channels=8, kernel_size=8),
            # 121x121
            torch.nn.ReLU(),
            torch.nn.ConvTranspose2d(in_channels=8, out_channels=1, kernel_size=8),
            # 128x128
            torch.nn.ReLU()
        )

        torch_utils.init_parameters(self)

    def forward(self, value):

        value = self.layers_encoder(value)

        value = value.view(-1, self.size_output_layers_encoder_prod)
        value = self.layers_fc(value)
        value = value.view(
            -1,
            self.size_output_layers_encoder[0],
            self.size_output_layers_encoder[1],
            self.size_output_layers_encoder[2]
        )
        value = self.layers_decoder(value)

        return value