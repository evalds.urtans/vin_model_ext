import torch
import torch.nn
import torch.nn.functional as F
import torch.utils.data
import numpy as np
import logging
import modules.torch_utils as torch_utils
from torch.autograd import Variable

class Model(torch.nn.Module):
    def __init__(self, args):
        super(Model, self).__init__()
        self.args = args

        # input 32 x 32

        self.layers_encoder = torch.nn.Sequential(
            torch.nn.Conv2d(in_channels=1, out_channels=4, kernel_size=3, padding=1, bias=True),
            torch.nn.ReLU(),
            torch.nn.Conv2d(in_channels=4, out_channels=8, kernel_size=3, padding=1, bias=False),
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=8),
            torch.nn.MaxPool2d(kernel_size=3, stride=2, padding=1),
            torch.nn.Conv2d(in_channels=8, out_channels=16, kernel_size=3, padding=1, bias=False),
            torch.nn.ReLU(),
            torch.nn.Conv2d(in_channels=16, out_channels=32, kernel_size=3, padding=1, bias=False),
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=32),
            torch.nn.MaxPool2d(kernel_size=3, stride=2, padding=1),
            torch.nn.Conv2d(in_channels=32, out_channels=32, kernel_size=3, padding=1, bias=False),
            torch.nn.ReLU()
        )

        self.size_output_layers_encoder = [32, 8, 8]
        self.size_output_layers_encoder_prod = int(np.prod(self.size_output_layers_encoder))

        self.compression_fc = 0.5
        self.fc_hidden_size = int(self.size_output_layers_encoder_prod * self.compression_fc)

        self.layers_fc = torch.nn.Sequential(
            torch.nn.Linear(in_features=self.size_output_layers_encoder_prod, out_features=self.fc_hidden_size),
            torch.nn.ReLU()
        )

        self.len_layers_rnn = 1
        self.layer_rnn = torch.nn.LSTM(
            input_size=self.fc_hidden_size,
            hidden_size=self.fc_hidden_size,
            num_layers=self.len_layers_rnn,
            batch_first=False
        )

        self.layers_fc_2 = torch.nn.Sequential(
            torch.nn.Linear(in_features=self.fc_hidden_size, out_features=self.size_output_layers_encoder_prod),
            torch.nn.ReLU()
        )

        # todo torch.nn.MaxUnpool2d()
        # https://discuss.pytorch.org/t/about-maxpool-and-maxunpool/1349
        # todo resnet

        self.layers_decoder = torch.nn.Sequential(
            torch.nn.ConvTranspose2d(in_channels=self.size_output_layers_encoder[0], out_channels=16, kernel_size=7, stride=1, padding=1, bias=False),
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=16),
            torch.nn.ConvTranspose2d(in_channels=16, out_channels=12, kernel_size=7, stride=1, padding=1, bias=False),
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=12),
            torch.nn.ConvTranspose2d(in_channels=12, out_channels=8, kernel_size=7, stride=1, padding=1, bias=False),
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=8),
            torch.nn.ConvTranspose2d(in_channels=8, out_channels=6, kernel_size=7, stride=1, padding=1, bias=False),
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=6),
            torch.nn.ConvTranspose2d(in_channels=6, out_channels=4, kernel_size=7, stride=1, padding=1, bias=False),
            torch.nn.ReLU(),
            torch.nn.BatchNorm2d(num_features=4),
            torch.nn.ConvTranspose2d(in_channels=4, out_channels=1, kernel_size=7, stride=1, padding=1, bias=False),
            torch.nn.ReLU()
        )

        torch_utils.init_parameters(self)

    def init_hidden(self, batch_size):

        # for dataparallel (batch_size, num_layers, hidden_dim)
        # but before input to RNN must have to be (num_layers, batch_size, hidden_dim)
        hidden_rnn = torch.zeros(batch_size, self.len_layers_rnn, self.fc_hidden_size)
        state_rnn = torch.zeros(batch_size, self.len_layers_rnn, self.fc_hidden_size)

        if self.args.is_cuda:
            hidden_rnn = hidden_rnn.cuda()
            state_rnn = state_rnn.cuda()

        return [hidden_rnn, state_rnn] # [ (batch_size, num_layers, hidden_dim), (batch_size, num_layers, hidden_dim) ]

    def forward(self, input_x, input_len, hidden):
        # input_x (batch, seq_padded, rgb, w, h)

        hidden_perm = []
        for i in range(len(hidden)):
            # (batch_size, seq, features) => (num_layers, batch_size, hidden_dim)
            hidden_perm.append(hidden[i].permute(1, 0, 2).contiguous())

        # optimization for parallel execution
        shape_input = input_x.size()
        len_max_seq = shape_input[1]
        len_batch = shape_input[0]

        # cut & pack
        values_cutted = []
        for idx_batch, each_seq_len in enumerate(input_len): # size of len_batch
            values_cutted.append(input_x[idx_batch][0:each_seq_len])
        value = torch.cat(values_cutted, dim=0)

        # (batch_size * seq, features)
        # encoder
        value = self.layers_encoder(value)
        value = value.view(-1, self.size_output_layers_encoder_prod)
        value = self.layers_fc(value)

        shape_after_encoder = value.size()

        # unpack & pad
        values_padded = []
        idx_cursor = 0
        for idx_batch, each_seq_len in enumerate(input_len):
            len_left = len_max_seq - each_seq_len
            zeros = torch.zeros((len_left, shape_after_encoder[1])).cuda()
            packed_value = torch.cat([value[idx_cursor:idx_cursor+each_seq_len], zeros], dim=0)
            packed_value = packed_value.unsqueeze(dim=0)
            values_padded.append(packed_value)
        value = torch.cat(values_padded, dim=0)

        # (batch, seq, features) => (seq, batch, features)
        value = value.permute(1, 0, 2).contiguous()

        # rnn
        packed = torch.nn.utils.rnn.pack_padded_sequence(value, input_len, batch_first=False) # CUDNN packing
        value_packed, hidden_perm = self.layer_rnn(packed, (hidden_perm[0], hidden_perm[1]))
        value, _ = torch.nn.utils.rnn.pad_packed_sequence(value_packed, batch_first=False) # CUDNN packing

        # (seq, batch, features) => (batch, seq, features)
        value = value.permute(1, 0, 2).contiguous()

        # cut & pack
        values_cutted = []
        for idx_batch, each_seq_len in enumerate(input_len): # size of len_batch
            values_cutted.append(value[idx_batch][0:each_seq_len])
        value = torch.cat(values_cutted, dim=0)

        # decoder
        value = self.layers_fc_2(value)
        value = value.view(
            -1,
            self.size_output_layers_encoder[0],
            self.size_output_layers_encoder[1],
            self.size_output_layers_encoder[2]
        )
        value = self.layers_decoder(value)

        shape_after_decoder = value.size()

        # unpack & pad
        values_padded = []
        idx_cursor = 0
        for idx_batch, each_seq_len in enumerate(input_len):
            len_left = len_max_seq - each_seq_len
            zeros = torch.zeros((len_left, shape_after_decoder[1], shape_after_decoder[2], shape_after_decoder[3])).cuda()
            packed_value = torch.cat([value[idx_cursor:idx_cursor+each_seq_len], zeros], dim=0)
            packed_value = packed_value.unsqueeze(dim=0)
            values_padded.append(packed_value)
        value = torch.cat(values_padded, dim=0)

        # value => (batch, seq, channel, w, h)

        hidden_out = []
        for i in range(len(hidden_perm)):
            hidden_out.append(hidden_perm[i].permute(1, 0, 2).contiguous())

        return value, hidden_out