import torch
import torch.nn
import torch.nn.functional as F
import torch.utils.data
import numpy as np
import logging
import modules.torch_utils as torch_utils


class Model(torch.nn.Module):
    def __init__(self, args):
        super(Model, self).__init__()
        self.args = args

        self.layers_encoder_1 = torch.nn.Sequential(
            torch.nn.Conv2d(in_channels=1, out_channels=8, kernel_size=4, stride=2, padding=1),
            # 16x16x8
            torch.nn.ReLU()
        )

        self.layers_encoder_2 = torch.nn.Sequential(
            torch.nn.Conv2d(in_channels=8, out_channels=16, kernel_size=4, stride=2, padding=1),
            # 8x8x16
            torch.nn.BatchNorm2d(num_features=16)
        )

        self.layers_encoder_3= torch.nn.Sequential(
            torch.nn.ReLU(),
            torch.nn.Conv2d(in_channels=16, out_channels=32, kernel_size=4, stride=2, padding=1),
            # 4x4x32
            torch.nn.BatchNorm2d(num_features=32),
            torch.nn.ReLU(),
            torch.nn.Conv2d(in_channels=32, out_channels=32, kernel_size=3, stride=1, padding=1),
            # 4x4x32
            torch.nn.BatchNorm2d(num_features=32)
        )

        self.size_output_layers_encoder = [32, 4, 4]
        self.size_output_layers_encoder_prod = int(np.prod(self.size_output_layers_encoder))

        self.compression_fc = 0.25
        self.fc_hidden_size = int(self.size_output_layers_encoder_prod * self.compression_fc)

        self.layers_fc = torch.nn.Sequential(
            torch.nn.Linear(in_features=self.size_output_layers_encoder_prod, out_features=self.fc_hidden_size),
            torch.nn.ReLU(),
            torch.nn.Linear(in_features=self.fc_hidden_size, out_features=self.size_output_layers_encoder_prod),
            torch.nn.ReLU()
        )

        self.layers_decoder_1 = torch.nn.Sequential(
            torch.nn.ConvTranspose2d(in_channels=32, out_channels=32, kernel_size=3, stride=2, padding=1),
            # 7x7x32
            torch.nn.BatchNorm2d(num_features=32),
            torch.nn.ReLU(),
            torch.nn.ConvTranspose2d(in_channels=32, out_channels=16, kernel_size=4, stride=1, padding=1),
            # 8x8x16
            torch.nn.BatchNorm2d(num_features=16)
        )

        self.layers_decoder_2 = torch.nn.Sequential(
            torch.nn.ConvTranspose2d(in_channels=16, out_channels=8, kernel_size=4, stride=2, padding=1),
            # 16x16x8
            torch.nn.BatchNorm2d(num_features=8)
        )

        self.layers_decoder_3 = torch.nn.Sequential(
            torch.nn.ConvTranspose2d(in_channels=8, out_channels=1, kernel_size=4, stride=2, padding=1),
            # 32x32x1
            torch.nn.BatchNorm2d(num_features=1),
            torch.nn.ReLU()
        )

        torch_utils.init_parameters(self)

    def forward(self, value):
        value = self.layers_encoder_1(value)
        res1 = value
        value = self.layers_encoder_2(value)
        res2 = value
        value = self.layers_encoder_3(value)

        value = value.view(-1, self.size_output_layers_encoder_prod)
        value = self.layers_fc(value)
        value = value.view(
            -1,
            self.size_output_layers_encoder[0],
            self.size_output_layers_encoder[1],
            self.size_output_layers_encoder[2]
        )

        value = self.layers_decoder_1(value)
        value = value + res2
        value = self.layers_decoder_2(value)
        value = value + res1
        value = self.layers_decoder_3(value)

        return value