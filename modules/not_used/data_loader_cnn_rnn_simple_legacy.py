import os
import torch
import torch.nn
import torch.nn.functional as F
import torch.utils.data
import json
import logging
import sklearn
import copy
import sklearn.model_selection
import numpy as np
import traceback, sys
import torch.nn.utils.rnn

class Dataset(torch.utils.data.dataset.Dataset):
    def __init__(self, maps_list, is_data_normalization, is_debug_data_loader):

        super(Dataset, self).__init__()

        self.data = []

        self.max_seq_size = 0
        for filename in maps_list:
            try:
                filename_each = filename + '/metadata.json'
                if os.path.exists(filename_each):
                    with open(filename_each, 'r') as fp:
                        metadata = json.load(fp)
                    checkpoint_iterations = [it for it in metadata['iterations'] if it['is_checkpoint']]
                    self.max_seq_size = max(len(checkpoint_iterations), self.max_seq_size)
            except Exception as e:
                logging.error(filename)
                logging.error(str(e))
                exc_type, exc_value, exc_tb = sys.exc_info()
                logging.error(traceback.format_exception(exc_type, exc_value, exc_tb))
        logging.info(f'max seq len: {self.max_seq_size}')

        if is_debug_data_loader:
            debug_map_idx = 0

        for filename in maps_list:
            try:
                filename_each = filename + '/metadata.json'
                if os.path.exists(filename_each):
                    with open(filename_each, 'r') as fp:
                        metadata = json.load(fp)

                    sequence = []

                    checkpoint_iterations = [it for it in metadata['iterations'] if it['is_checkpoint']]
                    # add 0-th iteration
                    checkpoint_iterations.insert(0, copy.copy(checkpoint_iterations[0]))
                    checkpoint_iterations[0]['iteration'] = 0

                    if is_debug_data_loader:
                        debug_map_seq_idx = debug_map_idx

                    for iteration in checkpoint_iterations:
                        iteration_id = str(iteration['iteration'])

                        filename_each = filename + f'/{iteration_id}.memmap'
                        if os.path.exists(filename_each):
                            step = np.memmap(
                                filename_each,
                                mode='r',
                                dtype=np.float16,
                                shape=tuple(metadata['memmap_shape']))

                            if is_data_normalization:
                                # normalize -1 to 1 for ReLU
                                vmax = np.amax(step)
                                vmin = np.amin(step)
                                vdelta = vmax - vmin
                                step = (step - vmin) / vdelta # 0.0 .. 1.0
                                step -= 0.5
                                step *= 2.0 # -1.0 .. 1.0

                            step = np.expand_dims(step, axis=0).astype(np.float) # needed for dataloader batching
                            if is_debug_data_loader:
                                step = np.ones_like(step) * float(debug_map_seq_idx)
                                debug_map_seq_idx += 1

                            sequence.append(step)
                        else:
                            logging.warning(f'missing iteration: {filename_each}')

                    if len(sequence) > 1:
                        self.data.append({
                            'x': sequence[0],
                            'y': sequence[-1],
                            'sequence': sequence,
                            'sequence_len': len(sequence)
                        })

                    if is_debug_data_loader:
                        debug_map_idx += 100
                    logging.info(f'filename: {filename} sequence_len: {len(sequence)}')
                else:
                    logging.warning(f'missing metadata file {filename_each}')

            except Exception as e:
                logging.error(filename)
                logging.error(str(e))
                exc_type, exc_value, exc_tb = sys.exc_info()
                logging.error(traceback.format_exception(exc_type, exc_value, exc_tb))

        logging.info('dataset size: {}'.format(len(self.data)))

    def __getitem__(self, index):
        result = self.data[index]
        return result


    def __len__(self):
        return len(self.data)


class DataLoader(object):

    # convert { (batch_size, x), (batch_size, y) } to { (x, batch_size), (y, batch_size), (seq, timesteps, batch_size) }
    @staticmethod
    def collate_fn(data):
        batch = {}

        # pad_packed_sequence requires max seq first
        data.sort(key=lambda it: it['sequence_len'], reverse=True)
        max_len_sequence = data[0]['sequence_len']

        shape_sequence = list(np.array(data[0]['sequence']).shape)
        shape_sequence[0] = max_len_sequence
        shape_sequence = tuple(shape_sequence)

        for key in data[0]:
            if key not in batch:
                batch[key] = []

            if key == 'sequence':
                # pad end with zeros
                for each in data:
                    seq = np.zeros(shape_sequence)
                    seq[0:each['sequence_len']] = each['sequence']
                    batch[key].append(seq)
            else:
                for each in data:
                    batch[key].append(each[key])
            batch[key] = np.array(batch[key])
            if key == 'sequence':
                batch[key] = batch[key].transpose((1, 0, 2, 3, 4)) # seq first then batch

        formated_batch = {
            'x': [],  # (seq, batch, features)
            'y': [],  # (seq, batch, features)
            'len': batch['sequence_len'] - 1  # SKIP first step (batch, len)
        }

        for idx_timestep in range(len(batch['sequence'])): # to max seq

            # skip first step, because we train on x = d_{t-1} and y = d_{t}
            if idx_timestep == 0:
                continue

            formated_batch['x'].append(batch['sequence'][idx_timestep - 1]) # append( (batch, features) (
            formated_batch['y'].append(batch['sequence'][idx_timestep])

        for key in formated_batch:
            formated_batch[key] = np.array(formated_batch[key])

        # (seq, batch, features)
        return formated_batch

    @staticmethod
    def get_data_loaders(args):

        maps_list = []
        for filename in os.listdir(args.path_value_maps):
            filename = args.path_value_maps + '/' + filename
            if os.path.isdir(filename):
                maps_list.append(filename)

        maps_list_train, maps_list_test = sklearn.model_selection.train_test_split(
            maps_list,
            test_size=0.2,
            shuffle=False)

        dataset_train = Dataset(maps_list_train, args.is_data_normalization, args.is_debug_data_loader)
        dataset_test = Dataset(maps_list_test, args.is_data_normalization, args.is_debug_data_loader)

        logging.info('train dataset')
        data_loader_train = torch.utils.data.DataLoader(
            dataset_train,
            collate_fn=DataLoader.collate_fn,
            batch_size=args.batch_size,
            #num_workers=16,
            shuffle=True)

        logging.info('test dataset')
        data_loader_test = torch.utils.data.DataLoader(
            dataset_test,
            collate_fn=DataLoader.collate_fn,
            batch_size=args.batch_size,
            #num_workers=16,
            shuffle=False)

        return data_loader_train, data_loader_test
