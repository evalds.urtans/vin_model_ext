import torch
import torch.nn
import torch.utils.data
import numpy as np
import modules.torch_utils as torch_utils
import torch.nn.functional as F
from modules.block_resnet_2d import BasicBlock2D
from modules.block_resnet_deconv2d import DeconvBlock2D


class Model(torch.nn.Module):
    def __init__(self, args):
        super(Model, self).__init__()
        self.args = args

        # input 32 x 32
        self.start_conv = torch.nn.Conv2d(in_channels=1, out_channels=16, kernel_size=4, bias=False)
        # 29x29x16
        self.block2d_1 = BasicBlock2D(in_channels=16, out_channels=16, stride=1)
        # 29x29x16
        self.block2d_2 = BasicBlock2D(in_channels=16, out_channels=32, stride=2)
        # 15x15x32
        self.block2d_3 = BasicBlock2D(in_channels=32, out_channels=32, stride=1)
        # 15x15x32
        self.block2d_4 = BasicBlock2D(in_channels=32, out_channels=64, stride=2, padding=1)
        # 9x9x64
        self.block2d_5 = BasicBlock2D(in_channels=64, out_channels=64, stride=1)
        # 9x9x64
        self.block2d_6 = BasicBlock2D(in_channels=64, out_channels=128, stride=2)
        # 5x5x128
        self.block2d_7 = BasicBlock2D(in_channels=128, out_channels=128, stride=2)
        # 3x3x128

        # size 29 to 9
        self.encoder_skip_after_1_to_4 = torch.nn.Conv2d(in_channels=16, out_channels=64, kernel_size=3, stride=3, dilation=2, bias=False)
        # size 15 to 5
        self.encoder_skip_after_3_to_6 = torch.nn.Conv2d(in_channels=32, out_channels=128, kernel_size=3, stride=3, bias=False)

        self.size_output_layers_encoder = [128, 3, 3]
        self.size_output_layers_encoder_prod = int(np.prod(self.size_output_layers_encoder))

        self.len_layers_rnn = self.args.rnn_layer_count
        self.layer_rnn = torch.nn.LSTM(
            input_size=self.size_output_layers_encoder_prod,
            hidden_size=self.size_output_layers_encoder_prod,
            num_layers=self.len_layers_rnn,
            batch_first=False
        )

        self.block_deconv2d_7 = DeconvBlock2D(in_channels=128, out_channels=128, stride=2)
        # 5x5x128
        self.block_deconv2d_6 = DeconvBlock2D(in_channels=128, out_channels=64, stride=2)
        # 9x9x64
        self.block_deconv2d_5 = DeconvBlock2D(in_channels=64, out_channels=64, stride=1)
        # 9x9x64
        self.block_deconv2d_4 = DeconvBlock2D(in_channels=64, out_channels=32, stride=2, padding=1)
        # 15x15x32
        self.block_deconv2d_3 = DeconvBlock2D(in_channels=32, out_channels=32, stride=1)
        # 15x15x32
        self.block_deconv2d_2 = DeconvBlock2D(in_channels=32, out_channels=16, stride=2)
        # 29x29x16
        self.block_deconv2d_1 = DeconvBlock2D(in_channels=16, out_channels=16, stride=1)
        # 29x29x16
        self.end_deconv = torch.nn.ConvTranspose2d(in_channels=16, out_channels=1, kernel_size=4, bias=False)
        # 32x32x1

        # size 5 to 15
        self.decoder_skip_after_7_to_4 = torch.nn.ConvTranspose2d(in_channels=128, out_channels=32, kernel_size=3, stride=3, bias=False)
        # size 9 to 29
        self.deocder_skip_after_5_to_2 = torch.nn.ConvTranspose2d(in_channels=64, out_channels=16, kernel_size=3, stride=3, dilation=2, bias=False)

        torch_utils.init_parameters(self) # moved. init before preload
        if not self.args.preload_weights_filename == "none":
            # false if models are different, example: conv preload to rnn model
            self.load_state_dict(torch.load(self.args.preload_weights_filename),strict=False)
            for name, param in self.named_parameters():
                if 'block2d' in name:
                    param.requires_grad = False
                elif 'start_conv' in name:
                    param.requires_grad = False

    def init_hidden(self, batch_size):

        # for dataparallel (batch_size, num_layers, hidden_dim)
        # but before input to RNN must have to be (num_layers, batch_size, hidden_dim)
        hidden_rnn = torch.zeros(batch_size, self.len_layers_rnn, self.size_output_layers_encoder_prod)
        state_rnn = torch.zeros(batch_size, self.len_layers_rnn, self.size_output_layers_encoder_prod)

        if self.args.is_cuda:
            hidden_rnn = hidden_rnn.cuda()
            state_rnn = state_rnn.cuda()

        return [hidden_rnn, state_rnn] # (batch_size, num_layers, hidden_dim)

    def forward(self, input_x, input_len, hidden):

        # (batch_size, seq, features) => (seq, batch_size, hidden_dim)

        hidden_perm = []
        for i in range(len(hidden)):
            # (batch_size, seq, features) => (num_layers, batch_size, hidden_dim)
            hidden_perm.append(hidden[i].permute(1, 0, 2).contiguous())

        # optimization for parallel execution
        shape_input = input_x.size()
        len_max_seq = shape_input[1]
        len_batch = shape_input[0]

        # combine batch with seq for parallel processing through encoder
        # (seq_padded * batch, rgb, w, h)
        value = input_x.reshape((len_max_seq * len_batch, shape_input[2], shape_input[3], shape_input[4]))

        indexes_values = []
        for idx, each_seq_len in enumerate(input_len): # size of len_batch
            each_seq_len = each_seq_len.type(torch.LongTensor)
            index_offset = len_max_seq * idx # offset of batch
            indexes_values += np.arange(index_offset, index_offset+each_seq_len).tolist() # indexes index_offset ... index_offset+each_seq_len => [0, 1, 2, 3,]
        indexes_values = torch.tensor(indexes_values)
        if self.args.is_cuda:
            indexes_values = indexes_values.cuda()
        value = value.index_select(dim=0, index=indexes_values) # reduce data passed through CNN to only sequences without padding

        # (batch_size * seq, features)
        # encoder
        unet_skip_1 = self.start_conv(value)
        unet_skip_2 = self.block2d_1(unet_skip_1)

        # skip connection 1
        encoder_skip_1 = self.encoder_skip_after_1_to_4(unet_skip_2)

        unet_skip_3 = self.block2d_2(unet_skip_2)
        unet_skip_4 = self.block2d_3(unet_skip_3)

        # skip connection 2
        encoder_skip_2 = self.encoder_skip_after_3_to_6(unet_skip_4)

        unet_skip_5 = self.block2d_4(unet_skip_4)
        # add skip connection
        unet_no_skip_5 = unet_skip_5 + encoder_skip_1
        unet_skip_6 = self.block2d_5(unet_no_skip_5)
        unet_skip_7 = self.block2d_6(unet_skip_6)
        # add skip connection
        unet_no_skip_7 = unet_skip_7 + encoder_skip_2
        value = self.block2d_7(unet_no_skip_7)

        value = value.view(-1, self.size_output_layers_encoder_prod)

        shape_after_encoder = value.size()
        # seq * batch, hidden_after_fc
        value_seq = torch.zeros((len_max_seq * len_batch, shape_after_encoder[1]))
        if self.args.is_cuda:
            value_seq = value_seq.cuda()
        for idx, idx_seq in enumerate(indexes_values):
            value_seq[idx_seq] = value[idx]

        #(batch_size, seq, features)
        value = value_seq.reshape((len_batch, len_max_seq, shape_after_encoder[1]))

        #(batch, seq, features) => (seq, batch, features)
        value = value.permute(1, 0, 2).contiguous()

        # rnn
        packed = torch.nn.utils.rnn.pack_padded_sequence(value, input_len, batch_first=False) # CUDNN packing
        value_packed, hidden_perm = self.layer_rnn(packed, hidden_perm)
        value, _ = torch.nn.utils.rnn.pad_packed_sequence(value_packed, batch_first=False) # CUDNN packing
        shape_after_rnn = value.size()

        # (seq, batch, features) => (batch, seq, features)
        value = value.permute(1, 0, 2).contiguous()
        value = value.reshape((len_max_seq * len_batch, shape_after_rnn[2]))

        value = value.index_select(dim=0, index=indexes_values)  # reduce data passed through CNN to only sequences without padding

        value = value.view(
            -1,
            self.size_output_layers_encoder[0],
            self.size_output_layers_encoder[1],
            self.size_output_layers_encoder[2]
        )

        value = self.block_deconv2d_7(value) + unet_skip_7
        # skip connection 1
        decoder_skip_1 = self.decoder_skip_after_7_to_4(value)

        value = self.block_deconv2d_6(value) + unet_skip_6
        value = self.block_deconv2d_5(value) + unet_skip_5
        # skip connection 1
        decoder_skip_2 = self.deocder_skip_after_5_to_2(value)

        value = self.block_deconv2d_4(value) + unet_skip_4
        value = self.block_deconv2d_3(value) + unet_skip_3 + decoder_skip_1
        value = self.block_deconv2d_2(value) + unet_skip_2
        value = self.block_deconv2d_1(value) + unet_skip_1 + decoder_skip_2
        value = F.sigmoid(self.end_deconv(value))

        shape_after_decoder = value.size()

        # unpack decoder
        value_seq = torch.zeros((len_batch * len_max_seq, shape_after_decoder[1], shape_after_decoder[2], shape_after_decoder[3]))
        if self.args.is_cuda:
            value_seq = value_seq.cuda()
        for idx, idx_seq in enumerate(indexes_values):
            value_seq[idx_seq] = value[idx]

        # batch, seq, channel, w, h
        value = value_seq.reshape((len_batch, len_max_seq, shape_after_decoder[1], shape_after_decoder[2], shape_after_decoder[3]))

        hidden_out = []
        for i in range(len(hidden_perm)):
            hidden_out.append(hidden_perm[i].permute(1, 0, 2).contiguous())

        return value, hidden_out