import torch
import torch.nn
import torch.utils.data
import numpy as np
import logging
import torch.nn.functional as F
from modules.block_resnet_2d import BasicBlock2D
from modules.block_resnet_deconv2d import DeconvBlock2D
import modules.torch_utils as torch_utils

class Model(torch.nn.Module):
    def __init__(self, args):
        super(Model, self).__init__()
        self.args = args

        # input 32 x 32
        self.start_conv = torch.nn.Conv2d(in_channels=1, out_channels=16, kernel_size=4, bias=False)
        # conv layer count = 1
        # 29x29x16
        self.block2d_1 = BasicBlock2D(in_channels=16, out_channels=16, stride=1)
        # conv layer count = 3
        # 29x29x16
        self.block2d_2 = BasicBlock2D(in_channels=16, out_channels=32, stride=2)
        # conv layer count = 8
        # 15x15x32
        self.block2d_3 = BasicBlock2D(in_channels=32, out_channels=32, stride=1)
        # conv layer count = 12
        # 15x15x32
        self.block2d_4 = BasicBlock2D(in_channels=32, out_channels=64, stride=2, padding=1)
        # conv layer count = 15
        # 9x9x64
        self.block2d_5 = BasicBlock2D(in_channels=64, out_channels=64, stride=1)
        # conv layer count = 19
        # 9x9x64
        self.block2d_6 = BasicBlock2D(in_channels=64, out_channels=128, stride=2)
        # conv layer count = 22
        # 5x5x128
        self.block2d_7 = BasicBlock2D(in_channels=128, out_channels=128, stride=2)
        # conv layer count = 25
        # 3x3x128

        # size 29 to 9
        self.encoder_skip_after_1_to_4 = torch.nn.Conv2d(in_channels=16, out_channels=64, kernel_size=3, stride=3,
                                                         dilation=2, bias=False)
        # size 15 to 5
        self.encoder_skip_after_3_to_6 = torch.nn.Conv2d(in_channels=32, out_channels=128, kernel_size=3, stride=3,
                                                         bias=False)


        self.block_deconv2d_7 = DeconvBlock2D(in_channels=128, out_channels=128, stride=2)
        # 5x5x128
        self.block_deconv2d_6 = DeconvBlock2D(in_channels=128, out_channels=64, stride=2)
        # 9x9x64
        self.block_deconv2d_5 = DeconvBlock2D(in_channels=64, out_channels=64, stride=1)
        # 9x9x64
        self.block_deconv2d_4 = DeconvBlock2D(in_channels=64, out_channels=32, stride=2, padding=1)
        # 15x15x32
        self.block_deconv2d_3 = DeconvBlock2D(in_channels=32, out_channels=32, stride=1)
        # 15x15x32
        self.block_deconv2d_2 = DeconvBlock2D(in_channels=32, out_channels=16, stride=2)
        # 29x29x16
        self.block_deconv2d_1 = DeconvBlock2D(in_channels=16, out_channels=16, stride=1)
        # 29x29x16
        self.end_deconv = torch.nn.ConvTranspose2d(in_channels=16, out_channels=1, kernel_size=4, bias=False)
        # 32x32x1

        # size 5 to 15
        self.decoder_skip_after_7_to_4 = torch.nn.ConvTranspose2d(in_channels=128, out_channels=32, kernel_size=3, stride=3, bias=False)
        # size 9 to 29
        self.deocder_skip_after_5_to_2 = torch.nn.ConvTranspose2d(in_channels=64, out_channels=16, kernel_size=3, stride=3, dilation=2, bias=False)

        if not self.args.preload_weights_filename == "none":
            self.load_state_dict(torch.load(self.args.preload_weights_filename))
            for name, param in self.named_parameters():
                if 'block2d' in name:
                    param.requires_grad = False
                elif 'start_conv' in name:
                    param.requires_grad = False
        torch_utils.init_parameters(self)


    def forward(self, value):

        unet_skip_1 = self.start_conv(value)
        unet_skip_2 = self.block2d_1(unet_skip_1)
        encoder_skip_1 = self.encoder_skip_after_1_to_4(unet_skip_2)
        unet_skip_3 = self.block2d_2(unet_skip_2)
        unet_skip_4 = self.block2d_3(unet_skip_3)
        encoder_skip_2 = self.encoder_skip_after_3_to_6(unet_skip_4)
        unet_skip_5 = self.block2d_4(unet_skip_4)
        unet_no_skip_5 = unet_skip_5 + encoder_skip_1
        unet_skip_6 = self.block2d_5(unet_no_skip_5)
        unet_skip_7 = self.block2d_6(unet_skip_6)
        unet_no_skip_7 = unet_skip_7 + encoder_skip_2
        value = self.block2d_7(unet_no_skip_7)

        value = self.block_deconv2d_7(value) + unet_skip_7
        decoder_skip_1 = self.decoder_skip_after_7_to_4(value)
        value = self.block_deconv2d_6(value) + unet_skip_6
        value = self.block_deconv2d_5(value) + unet_skip_5
        decoder_skip_2 = self.deocder_skip_after_5_to_2(value)
        value = self.block_deconv2d_4(value) + unet_skip_4
        value = self.block_deconv2d_3(value) + unet_skip_3 + decoder_skip_1
        value = self.block_deconv2d_2(value) + unet_skip_2
        value = self.block_deconv2d_1(value) + unet_skip_1 + decoder_skip_2
        value = F.sigmoid(self.end_deconv(value))

        return value