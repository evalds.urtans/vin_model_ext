#!/bin/sh

module load conda
export TMPDIR=$HOME/tmp
source activate conda_env

export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
#locale-gen en_US.UTF-8

tensorboard --logdir ./runs --port 8080
