import os
import numpy as np
import time
import argparse
import json

import torch.utils.data
from PIL import Image
import matplotlib.pyplot as plt
import cv2
import matplotlib.cm as cm
import colorlover as cl

from modules.dict_to_obj import DictToObj
from modules.grad_cam import (
    BackPropagation,
    Deconvnet,
    GradCAM,
    GuidedBackPropagation,
    occlusion_sensitivity,
)

def load_image(filename, rnn):
    step = args.cell_size
    my_image = Image.open(filename)
    map_size = [int(i / step) for i in my_image.size]
    new_image = np.zeros((map_size[0],map_size[1],))
    for x in range(0, my_image.size[0], step):
        for y in range(0, my_image.size[1], step):
            pixel = my_image.getpixel((y + step / 2, x + step / 2))
            if list(pixel) ==[0, 0, 0]:
                new_image[int(x / step)][int(y / step)] = 0
            elif list(pixel) == [255, 255, 255]:
                new_image[int(x / step)][int(y / step)] = 0.5
            elif list(pixel) == [255, 0, 0]:
                new_image[int(x / step)][int(y / step)] = 1
    new_image = np.expand_dims(new_image, 0)
    new_image = np.expand_dims(new_image, 0)
    if rnn == True:
        new_image = np.expand_dims(new_image, 0)
    return new_image

def save_gradient(filename, gradient):
    gradient = gradient.detach().cpu().numpy()[0].transpose(1, 2, 0)

    gradient -= gradient.min()
    gradient /= gradient.max()
    gradient *= 255.0
    cv2.imwrite(filename, np.uint8(gradient))

def save_gradcam(filename, gcam, raw_image, paper_cmap=False):
    gcam = gcam.cpu().numpy()[0][0]
    raw_image = raw_image[0][0]
    cmap = cm.jet_r(gcam)[..., :3] * 255.0
    raw_image = cm.jet_r(raw_image)[..., :3] * 255.0
    if paper_cmap:
        alpha = gcam[..., None]
        gcam = alpha * cmap + (1 - alpha) * raw_image
    else:
        gcam = (cmap.astype(np.float) + raw_image.astype(np.float)) / 2
    cv2.imwrite(filename, np.uint8(gcam))

def get_path(input_map, real_map):
    while True:
        time.sleep(0.01)
        rand_x = np.random.randint(0, 32)
        rand_y = np.random.randint(0, 32)
        if real_map[rand_y, rand_x] == 0.5:
            break


    return input_map, [rand_y, rand_x]


if __name__ == '__main__': # avoid problems with spawned processes
    parser = argparse.ArgumentParser(description='Model trainer')

    parser.add_argument('-is_cuda', default=False, type=lambda x: (str(x).lower() == 'true'))

    parser.add_argument('-preload_dir', default="none", type=str)

    parser.add_argument('-path_value_map', default='../VIN_Research_ValueIterationGridmap/valueMaps', type=str)
    parser.add_argument('-cell_size', default=20, type=int)

    parser.add_argument('-rnn_convergence_limit', default=0.01, type=float)
    parser.add_argument('-grad_cam_dir', default="grad_cam", type=str)

    args, args_other = parser.parse_known_args()

    # load model json
    args = args.__dict__
    with open(os.path.join(args['preload_dir'], 'model_desc.json')) as f:
        desc = json.load(f)
        for key in desc.keys():
            if key not in args.keys():
                args[key] = desc[key]
    args = DictToObj(**args)

    if not torch.cuda.is_available():
        args.is_cuda = False

    Model = getattr(__import__('modules.' + desc['model'], fromlist=['Model']), 'Model')
    model = Model(args)

    if args.is_cuda:
        model.load_state_dict(torch.load(os.path.join(args.preload_dir, 'best.pt')))
        model = model.cuda()
    else:
        model.load_state_dict(torch.load(os.path.join(args.preload_dir, 'best.pt'), map_location = 'cpu'))
        model = model.cpu()

    model = model.eval()

    plt.show()
    fig, ax = plt.subplots(nrows=2, ncols=2)
    cmap = plt.get_cmap('gray')

    if 'rnn' in args.model:
        image = load_image(args.path_value_map, rnn=True)
        input = torch.tensor(image).type(torch.FloatTensor)
        input_len = torch.tensor(np.ones((1,))).type(torch.IntTensor)
        if args.is_cuda:
            input = input.cuda()
            input_len = input_len.cuda()
        hidden = model.init_hidden(batch_size=1)
        max_iterations = 50
        plt.imshow(np.asarray(input)[0, 0, 0], cmap=cmap)
        plt.draw()
        plt.pause(0.1)
        total_time = 0
        for iter_count in range(max_iterations):
            start_time = time.time()
            output, hidden = model.forward(input, input_len, hidden)
            print(f'time: {time.time()-start_time}')
            total_time += time.time()-start_time
            input += output
            output_map = np.asarray(input.detach().cpu())
            plt.imshow(output_map[0,0,0], cmap=cmap)
            plt.draw()
            plt.pause(0.1)
        print(f'total time {total_time}')
        # plt.imshow(output_map[0, 0, 0], cmap=cmap)
        # plt.draw()
    else:
        image = load_image(args.path_value_map, rnn=False)
        input = torch.tensor(image).type(torch.FloatTensor)
        if args.is_cuda:
            input = input.cuda()

        gcam = GradCAM(model=model)
        _ = gcam.forward(input)
        gcam.backward(np.unravel_index(np.argmax(_.detach()), _.detach().shape))
        gradients = gcam.generate(target_layer='block_deconv2d_7')
        save_gradcam(
            filename=os.path.join(
                args.grad_cam_dir,
                "deconv_image1.png"
            ),
            gcam=gradients,
            raw_image=image
        )
        # draw input image
        ax[0][0].imshow(np.asarray(input)[0, 0], cmap=cmap)
        ax[0][0].set_title("input image")
        plt.draw()

        start_time = time.time()
        output = model.forward(input)
        print(f'time: {time.time()-start_time}')
        output_map = np.asarray(output.detach().cpu())[0, 0]
        path_map = get_path(output_map, image[0, 0])
        # save_gradient(
        #     filename=os.path.join(
        #         args.grad_cam_dir,
        #         "real_image.png"
        #     ),
        #     gradient=output,
        # )
        # draw output map
        cmap = plt.get_cmap('hot')
        ax[0][1].imshow(output_map, cmap=cmap)
        ax[0][1].set_title("output image")
        # draw path from random coords
        path_map, coords = get_path(output_map, image[0, 0])
        ax[1][1].imshow((path_map), cmap=cmap)
        ax[1][1].set_title(f'path from: {coords}')

        plt.tight_layout(True)
        plt.show()